import * as VueRouter from "vue-router";
import Main from "@view/main/index.vue";
import Point from "@view/example/base/point/index.vue";
import Line from "@view/example/base/line/index.vue";
import Cube from "@view/example/base/cube/index.vue";
import Triangles from "@view/example/base/Triangles/index.vue";
import Sprite from "@view/example/base/Sprite/index.vue";
import Rectangle from "@view/example/base/Rectangle/index.vue";
import cube_texture from "@view/example/base/cube_texture/index.vue";


import OrthographicCamera from "@view/example/Camera/OrthographicCamera/index.vue";
import PerspectiveCamera from "@view/example/Camera/PerspectiveCamera/index.vue";

import LightedCube from "@view/example/light/LightedCube/index.vue";
import LisghtedCube_ambient from "@view/example/light/LisghtedCube_ambient/index.vue";
import LightedTranslatedRotatedCube from "@view/example/light/LightedTranslatedRotatedCube/index.vue";
import PointLightedCode from "@view/example/light/PointLightedCode/index.vue";
import PointLightedCodeFrgments from "@view/example/light/PointLightedCode_preFagment/index.vue";

import jointMode from "@view/example/theHierarchicalModel/jointMode/index.vue";
import multiJoinModel from "@view/example/theHierarchicalModel/multiJoinModel/index.vue";

import pickingCude from "@view/example/pickingSystem/pickingCude/index.vue";

console.log("LookAt:");
// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
//默认的路由
const defaultRoutes = [{ path: "/", component: Main, mate: { title: "首页" } }];
//例子中的路由
const exampleRoutes = [
    { path: "/example/base/point", component: Point, mate: { title: "点" } },
    { path: "/example/base/line", component: Line, mate: { title: "线" } },
    { path: "/example/base/Triangles", component: Triangles, mate: { title: "三角形" } },
    { path: "/example/base/Rectangle", component: Rectangle, mate: { title: "矩形" } },
    { path: "/example/base/Sprite", component: Sprite, mate: { title: "精灵" } },
    { path: "/example/base/cube", component: Cube, mate: { title: "正方体" } },
    { path: "/example/base/cube_texture", component: cube_texture, mate: { title: "正方体带贴图" } },
    

];
const CameraRoutes = [
    { path: "/example/Camera/OrthographicCamera", component: OrthographicCamera, mate: { title: "正射投影相机" } },
    { path: "/example/Camera/PerspectiveCamera", component: PerspectiveCamera, mate: { title: "正交投影相机" } },
];
const LightRoutes = [
    { path: "/example/light/LightedCube", component: LightedCube, mate: { title: "静态下平行光下的漫反射" } },
    { path: "/example/light/LisghtedCube_ambient", component: LisghtedCube_ambient, mate: { title: "静态下平行光下+环境光+漫反射" } },
    { path: "/example/light/LightedTranslatedRotatedCube", component: LightedTranslatedRotatedCube, mate: { title: "动态下平行光下+环境光+漫反射" } },
    { path: "/example/light/PointLightedCode", component: PointLightedCode, mate: { title: "点光源-逐顶点" } },
    { path: "/example/light/PointLightedCodeFrgments", component: PointLightedCodeFrgments, mate: { title: "点光源-逐片元" } },
];

const TheHierarchicalModelRoutes = [ 
    { path: "/example/theHierarchicalModel/jointMode", component: jointMode, mate: { title: "双层级" } },
    { path: "/example/theHierarchicalModel/multiJoinModel", component: multiJoinModel, mate: { title: "多层级" } },
];
const PickingSystem = [ 
    { path: "/example/pickingSystem/pickingCude", component: pickingCude, mate: { title: "拾取" } },
 
];

const exampleRoutesList = [
    {
        title: "首页",
        children: defaultRoutes,
    },
    {
        title: "基础例子",
        children: exampleRoutes,
    },
    {
        title: "相机",
        children: CameraRoutes,
    },
    {
        title: "光",
        children: LightRoutes,
    },
    {
        title: "层次模型",
        children: TheHierarchicalModelRoutes,
    },
    {
        title: "拾取系统",
        children: PickingSystem,
    }
];

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = VueRouter.createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: VueRouter.createWebHashHistory(),
    routes: exampleRoutesList, // `routes: routes` 的缩写
});
export { defaultRoutes, exampleRoutes, exampleRoutesList };
export default router;
