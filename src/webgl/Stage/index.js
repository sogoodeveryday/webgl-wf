/**
 * 当创建一个 Stage 组件时，你可以考虑以下功能：

    添加场景：提供方法用于向舞台中添加场景，可以是3D场景、2D场景等。

    设置背景：提供方法用于设置舞台的背景颜色或背景图片。

    控制渲染：提供方法用于控制舞台的渲染，例如开始渲染、停止渲染等操作。

    响应交互：提供方法用于处理舞台中的交互操作，例如鼠标点击、键盘输入等。
 * 
 * 
 */
import { componentType, math, windowEvent } from "@webgl/utils/index.js";
export default class Stage {
    /**
     * 设置gl一些基本属性
     * @typedef {Object} config
     * @property {document} el canvas
     * @property {number} width 要设置的canvas与gl的宽度
     * @property {number} height 要设置的canvas与gl的高度
     * @constructor
     */
    constructor(config) {
        this.className = componentType.stage;
        this.uuid = math.generateUUID(this.calssName);
        //场景队列
        this.sceneList = [];
        //updata队列
        this.updateList = [];
        this.camera = {};
        //存方灯光的队列 因为有时候场景会存在多个灯光
        this.lightList = [];
        //gl 实例
        this.gl = null;
        //canvase lement
        this.canvas = config.el;
        // this.renderer = new Renderer(config);
        this.init(config);
    }
    /**
     * webgl的上下文
     * @param {WebGL3DRenderingContext} gl
     * @returns
     */
    setGl(gl) {
        this.gl = gl;
    }
    /**
     * 设置gl一些基本属性
     * @typedef {Object} config
     * @property {document} el canvas
     * @property {number} width 要设置的canvas与gl的宽度
     * @property {number} height 要设置的canvas与gl的高度
     * @returns
     */
    init(config) {
        let { el, width, height } = config;
        let gl = el.getContext("webgl");
        el.width = width;
        el.height = height;
        windowEvent.handleResize((data) => {
            let { clientHeight, clientWidth } = data;
            console.log(clientHeight, clientWidth, data);
            el.width = clientWidth;
            el.height = clientHeight;
        });
        gl.viewport(0, 0, width, height);
        this.setGl(gl);
        return this;
    }
    //添加一个创景的组件
    addScene(scene) {
        this.sceneList.push(scene);
        return this;
    }
    //帧刚更新
    addUpdate(updata) {
        this.updateList.push(updata);
    }
    /**
     * 绑定相机
     * @param {Object} camera
     */
    bindCamera(camera) {
        console.log("bindCamera:", camera);
        this.camera = camera || null;
    }
    addLight(light) {
        this.lightList.push(light);
        return this;
    }
    //更新场景
    update(stage) {
        let { gl } = stage;
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.enable(gl.DEPTH_TEST | gl.POLYGON_OFFSET_FIL);
        // gl.enable(L); //开始多边形偏移
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        const { sceneList, updateList } = this;
        for (let i = 0; i < sceneList.length; i++) {
            const item = sceneList[i];
            gl.polygonOffset(1.0, 1.0);
            item.update && item.update(stage);
        }
        for (let i = 0; i < updateList.length; i++) {
            updateList[i](stage);
        }
    }
}
