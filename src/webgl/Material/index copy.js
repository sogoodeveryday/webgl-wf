/**
 * 材质（Material）组件通常需要处理以下情况：

设置着色器程序：负责加载顶点着色器和片段着色器，并创建着色器程序。
设置材质属性：例如颜色、纹理、光照属性等。
提供方法用于在渲染过程中将材质属性传递给着色器程序。
管理材质的渲染状态，例如启用/禁用深度测试、混合等。
处理材质的光照计算，例如在着色器程序中实现光照模型。
可能需要处理材质的纹理加载和映射。

这些是材质组件通常需要处理的情况，具体实现会根据渲染引擎的需求和设计进行扩展和优化。
Material组件定义了使用哪个Shader来渲染物体，并且可以设置Shader中的参数，比如颜色、纹理、透明度等
 * 
 */
import { objectHaveVlaue, isObject } from "@webgl/utils/index.js";
import { Program } from "@webgl/index.js";
export class Material {
    constructor(config) {
        this.gl = config.gl;
        this.uniform = {}; // WebGL uniform 变量
        this.attribute = {}; //wengl attribute 变量
        this.program = {};
        // 其他材质属性的初始化
        this.init(config);
    }
    /**
     * 创建着色器
     * @typeof {Object} config 传递的配置
     * @param {WebGLRenderingContext} config.gl 传递的配置
     * @param {string} config.vertexShaderSource 顶点着色器代码
     * @param {string} config.fragmentShaderSource 片元着色器代码
     * @typeof {object} uniform 要传递的着色器中相关参数的表变量 {key:value}的形式
     * @return Material
     */
    init({ gl, vertexShaderSource, fragmentShaderSource, uniform, attribute }) {
        if (arguments.length === 0) return;
        this.setGl(gl);
        objectHaveVlaue(uniform) && this.setUniform(uniform);
        vertexShaderSource && fragmentShaderSource && this.createProgram({ vertexShaderSource, fragmentShaderSource });
    }
    /**
     *
     * @param {WebGLRenderingContext} gl 上下文
     */
    setGl(gl) {
        this.gl = gl ? gl : this.gl;
        return this;
    }
    /**
     * 传递shader中的相关参数
     * @param {Object} uniform 要传递的着色器中相关参数的表变量 {key:value}的形式
     */
    setUniform(uniform = {}) {
        if (!objectHaveVlaue(uniform)) return;
        let oldUniform = this.uniform;
        let newUniform = Object.assign(oldUniform, uniform);
        this.uniform = newUniform;
        this.setUniformData(newUniform);
        return this;
    }
    setUniformData(uniform) {
        // debugger;
        let info = this.getProgramData();
        let bool = objectHaveVlaue(info);
        if (!bool) return;
        let uniformData = this.getProgramData().uniformData;
        let keys = Object.keys(uniform);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let valueOrObj = uniform[key];
            let item = uniformData[key];
            if (!item) return;
            if (isObject(valueOrObj)) {
                item = Object.assign(item, valueOrObj);
            } else if (item && value) {
                console.log("value:", value);
                item["value"] = uniform[key];
            }
        }
    }
    /**
     * 传递shader中的相关参数
     * @param {Object} attribute 要传递的着色器中相关参数的表变量 {key:value}的形式
     */
    setAttribute(attribute = {}) {
        if (!objectHaveVlaue(attribute)) return;
        let oldAttribute = this.attribute;
        let newAttribute = Object.assign(oldAttribute, attribute);
        this.attribute = newAttribute;
        this.setAttributeData(newAttribute);
        return this;
    }
    setAttributeData(attribute) {
        let info = this.getProgramData();
        let bool = objectHaveVlaue(info);
        if (!bool) return;
        let attributeData = this.getProgramData().attributeData;
        let keys = Object.keys(attribute);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let item = attributeData[key];
            let valueOrObj = attribute[key];
            if (!item) return;
            if (isObject(valueOrObj)) {
                item = Object.assign(item, valueOrObj);
            } else if (item && value) {
                console.log("value:", value);
                item["value"] = uniform[key];
            }
        }
    }
    /**
     * @typeof {Object} config 传递的配置
     * @param {string} config.vertexShaderSource 顶点着色器代码
     * @param {string} config.fragmentShaderSource 片元着色器代码
     * @returns
     */
    createProgram({ vertexShaderSource, fragmentShaderSource }) {
        let boo = vertexShaderSource && fragmentShaderSource;
        if (!boo) return console.warn("并未传递顶点着色器或者片元着色器，请检测并传入"); //不存在就直接返回
        let gl = this.gl;
        let config = {
            gl,
            vertexShaderSource,
            fragmentShaderSource,
        };
        console.log("this.program:", this.program);
        this.program = new Program(config);
        this.setUniform(this.uniform);
        this.setAttribute(this.uniform);
        // debugger;
        return this;
    }
    getProgramData() {
        return this.program?.programData;
    }
    /**
     *设置片元着色器与顶点着色器
     * @param {string} vertexShaderSource 顶点着色器代码
     * @param {string} fragmentShaderSource 片元着色器代码
     * @returns
     */
    setGLSLSource(vertexShaderSource, fragmentShaderSource) {
        this.createProgram({ vertexShaderSource, fragmentShaderSource });
        return this;
    }
    setRenderState(renderState) {
        // 设置渲染状态
    }
    render(renderState) {
        // 渲染
    }
    destroy() {
        // 销毁材质
    }

    // 其他材质属性和方法
}
