/**
 * 材质（Material）组件通常需要处理以下情况：

设置着色器程序：负责加载顶点着色器和片段着色器，并创建着色器程序。
设置材质属性：例如颜色、纹理、光照属性等。
提供方法用于在渲染过程中将材质属性传递给着色器程序。
管理材质的渲染状态，例如启用/禁用深度测试、混合等。
处理材质的光照计算，例如在着色器程序中实现光照模型。
可能需要处理材质的纹理加载和映射。

这些是材质组件通常需要处理的情况，具体实现会根据渲染引擎的需求和设计进行扩展和优化。
Material组件定义了使用哪个Shader来渲染物体，并且可以设置Shader中的参数，比如颜色、纹理、透明度等
 * 
 */
import { objectHaveVlaue, isObject } from "@webgl/utils/index.js";
import { Program } from "@webgl/index.js";
export class Material {
    constructor(config) {
        this.gl = config.gl;
        this.uniforms = {}; // WebGL uniform 变量
        this.program = {};
        this.vertexShaderSource = "";
        this.fragmentShaderSource = "";
        // 其他材质属性的初始化
        this.init(config);
    }
    /**
     * 创建着色器
     * @typeof {Object} config 传递的配置
     * @param {WebGLRenderingContext} config.gl 传递的配置
     * @param {string} config.vertexShaderSource 顶点着色器代码
     * @param {string} config.fragmentShaderSource 片元着色器代码
     * @typeof {object} uniform 要传递的着色器中相关参数的表变量 {key:value}的形式
     * @return Material
     */
    init({ gl, vertexShaderSource, fragmentShaderSource, uniforms = {} }) {
        if (arguments.length === 0) return;
        this.setGl(gl);
        this.uniforms = uniforms;
        vertexShaderSource && fragmentShaderSource && this.createProgram(vertexShaderSource, fragmentShaderSource);
    }
    /**
     *
     * @param {WebGLRenderingContext} gl 上下文
     */
    setGl(gl) {
        this.gl = gl ? gl : this.gl;
        return this;
    }
    setUniformFormat(uniforms) {
        this.uniforms = { ...this.uniforms, ...uniforms };
        this.mixUniformFormat();
        return this;
    }
    mixUniformFormat() {
        //存在uniforms 
        if (!objectHaveVlaue(this.uniforms)) return;
        //获取uniformData内容
        let uniformData = this.getUniformsData();
        if (!uniformData) return;
        //获取uniformData的key值方便遍历
        let keys = Object.keys(uniformData);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            //获取到uniformData中获取的变量
            let item = uniformData[key];
            //对变量的赋值
            let item2 = this.uniforms[key];
            if (item2 && item) {
                let timeData = Object.assign(item, item2);
                let { valueType } = timeData;
                valueType > 1 && (timeData.value = new Float32Array(timeData.value));
                this.uniforms[key] = timeData;
            }
        }

        return this;
    }
    /**
     * @typeof {Object} config 传递的配置
     * @param {string} config.vertexShaderSource 顶点着色器代码
     * @param {string} config.fragmentShaderSource 片元着色器代码
     * @returns
     */
    createProgram(vertexShaderSource, fragmentShaderSource) {
        let boo = vertexShaderSource && fragmentShaderSource;
        if (!boo) return console.warn("并未传递顶点着色器或者片元着色器，请检测并传入"); //不存在就直接返回
        this.vertexShaderSource = vertexShaderSource;
        this.fragmentShaderSource = fragmentShaderSource;
        let gl = this.gl;
        let config = { gl, vertexShaderSource, fragmentShaderSource };
        this.program = new Program(config);
        //创建program之后 将变量混合起来
        this.setUniformFormat();
        return this;
    }
    getProgramData() {
        return this.program?.programData;
    }
    getUniformsData() {
        return this.getProgramData()?.uniformData;
    }
    /**
     *设置片元着色器与顶点着色器
     * @param {string} vertexShaderSource 顶点着色器代码
     * @param {string} fragmentShaderSource 片元着色器代码
     * @returns
     */
    setGLSLSource(vertexShaderSource, fragmentShaderSource) {
        this.createProgram(vertexShaderSource, fragmentShaderSource);
        return this;
    }
    setRenderState(renderState) {
        // 设置渲染状态
    }
    render(renderState) {
        // 渲染
    }
    destroy() {
        // 销毁材质
    }
    /**
     * 默认设置
     * @param {Object} obj
     * @returns
     */
    _setVttributeDefaultFormat(target) {
        let gl = this.gl;
        return {
            index: 0, //shader的变量名地址
            size: 0,
            type: gl.FLOAT,
            normalized: false,
            stride: 0,
            offse: 0,
            target,
            buffer: null,
            drawType: gl.STATIC_DRAW,
        };
    }

    // 其他材质属性和方法
}
