import { arrayLength } from "@webgl/utils/index.js";
export default class Mesh {
    /**
     * 初始化Mesh
     * @param {WebGLRenderingContext} gl
     * @param {Geometry} geometry 几何图形
     * @param {Material} material 材质
     */
    constructor({ gl, geometry, material }) {
        this.gl = gl;
        this.geometry = geometry;
        this.material = material;
        this.init({ gl, geometry, material });
        console.log(geometry, material);
    }
    /**
     *初始化Mesh
     * @param {WebGLRenderingContext} gl
     * @param {Geometry} geometry 几何图形
     * @param {Material} material 材质
     */
    init({ gl, geometry, material }) {
        let { program } = material;
        let {
            programData: { attributeData },
        } = program;
        let { vttributeInfo } = geometry;
        this.makevttribute(vttributeInfo, attributeData);
    }
    makevttribute(data, format) {
        let keys = Object.keys(format);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let item = format[key];
            if (data[key]) {
                item = Object.assign(item, data[key]);
                //将处理后的地址给了vttributeInfo
                //使用指针就能直接处理这里的值的内容
                data[key] = item;
            }
        }
    }
    /**
     * 更新集合体
     * @param {Stage} baseInfo
     * @returns
     */
    update = (baseInfo) => {
        /**
         * 需要优化的点是(a)的标记  最好是触发是数值更新的时候,赋值,而不是遍历渲染的时候
         */
        let { gl, drawGeometryType, camera, lightList } = baseInfo;
        let attributeData = this.material?.program?.programData?.attributeData;
        if (!attributeData) return console.log("attributeData is null");
        let uniforms = this.material.uniforms;
        if (!uniforms) return console.log("uniforms is null");
        let indexInfo = this.geometry.indexInfo;
        //赋值 投影矩阵 视图矩阵 (a)
        camera && camera?.update(this);
        //更新灯光的数值  (a)
        if (lightList && lightList.length > 0) {
            for (let index = 0; index < lightList.length; index++) {
                lightList[index].update(this);
            }
        }
        //使用当前的着色器片段
        this.material.program.useProgram();
        //处理unitfrom 变量
        let uniformKeys = Object.keys(uniforms);
        for (let i = 0; i < uniformKeys.length; i++) {
            let key = uniformKeys[i];
            let item = uniforms[key];
            let { location, setter, value, valueType } = item; 
            //贴图的变量赋值
            if (valueType === "sampler2D") {
                value ? value.bind(location) : console.error("value is null");
            } else { 
                //非贴图的变量赋值
                setter ? setter(gl, location, value) : console.error("setter is null", item);
            }
        }

        let attributeDatArrray = Object.keys(attributeData);
        //处理attribute 变量
        for (let i = 0; i < attributeDatArrray.length; i++) {
            let key = attributeDatArrray[i];
            let item = attributeData[key];
            let { location, size, buffer, target, type } = item;
            gl.bindBuffer(target, buffer);
            gl.vertexAttribPointer(location, size, type, false, 0, 0);
            gl.enableVertexAttribArray(location);
        }

        //非index变量绘制
        if (!indexInfo) {
            let item = attributeData["a_Position"];
            let { size, value } = item;
            gl.drawArrays(drawGeometryType, 0, value.length / size);
            return;
        }
        //index的变量绘制
        gl.bindBuffer(indexInfo.target, indexInfo.buffer);
        gl.drawElements(drawGeometryType, indexInfo.value.length, indexInfo.type, 0);
    };
}
