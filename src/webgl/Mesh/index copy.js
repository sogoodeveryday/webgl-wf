import { arrayLength } from "@webgl/utils/index.js";
export default class Mesh {
    /**
     * 初始化Mesh
     * @param {WebGLRenderingContext} gl
     * @param {Geometry} geometry 几何图形
     * @param {Material} material 材质
     */
    constructor({ gl, geometry, material }) {
        this.gl = gl;
        this.geometry = geometry;
        this.material = material;
        this.bufferObj = {}; //储存了所有的buffer与对应的变量信息
        this.init({ gl, geometry, material });
        console.log(geometry, material);
    }
    /**
     *初始化Mesh
     * @param {WebGLRenderingContext} gl
     * @param {Geometry} geometry 几何图形
     * @param {Material} material 材质
     */
    init({ gl, geometry, material }) {
        let { program } = material;
        let {
            programData: { attributeData, uniformData },
        } = program;
        let { vttributeInfo } = geometry;
        this.makevttribute(vttributeInfo, attributeData);
        // let verticesInfo = geometry.verticesInfo;
        // let indices = geometry.indices;
        // if (arrayLength(indices)) {
        // } else {
        //     let attributeData = material.program.programData.attributeData;
        //     if (attributeData?.a_Position) {
        //         attributeData.a_Position.value = verticesInfo.value;
        //         attributeData.a_Position = Object.assign(attributeData.a_Position, verticesInfo.format);
        //     }
        // }
        /**设置buffer内容 */
        // this.makeShaderBufferData(material.program.programData.attributeData);
        // this.makeShaderBufferData(material.program.programData.uniformData);
    }
    makevttribute(data, format) {
        let keys = Object.keys(format);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let item = format[key]; 
            if (data[key]) item = Object.assign(item, data[key]);
            // let { name, type, size, normalized } = item;
            // let glType = gl[type];
            // let glSize = size;
            // let glNormalized = normalized ? gl.TRUE : gl.FALSE;
            // let glTypeSize = glType * glSize;
            // let glTypeSizeNormalized = glTypeSize * glNormalized;
            // let glTypeSizeNormalizedOffset = glTypeSizeNormalized * offset;
            // gl.bindBuffer(gl.ARRAY_BUFFER, this.glBuffer);
            // gl.vertexAttribPointer(name, glTypeSizeNormalizedOffset, gl.FLOAT, false, 0, 0);
            // gl.enableVertexAttribArray(name);
        }
    }
    makeUniform(data, format) {}
    /**
     * 设置attributeData中的数据
     * @param {*} data
     */
    makeShaderBufferData(data) {
        let array = Object.keys(data);
        for (let i = 0; i < array.length; i++) {
            let key = array[i];
            let item = data[key];
            let value = item?.value;
            let target = item?.target;
            let name = item?.name;
            if (value) {
                // debugger;
                item["buffer"] = this.createArrayBuffer(this.gl, target, value);
                this.bufferObj[name] = item;
                console.log("this.bufferObj:", this.bufferObj);
            }
        }
    }
    /**顶点数据 */
    createArrayBuffer(gl, target, data) {
        data = new Float32Array(data);
        return this.createBuffer(gl, target, data);
    }
    /**index的下角标数据 */
    createIndexbuffer(gl, data) {
        data = new Float32Array(data);
        return this.createBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, data);
    }
    createBuffer(gl, type, data) {
        const buffer = gl.createBuffer();
        gl.bindBuffer(type, buffer);
        gl.bufferData(type, data, gl.STATIC_DRAW);
        gl.bindBuffer(type, null);
        return buffer;
    }

    render = ({ gl }) => {
        //使用当前的着色器片段
        this.material.program.useProgram();
        // debugger;
        let bufferArray = Object.keys(this.bufferObj);
        // debugger;
        //向对应的着色器中传递相关数据
        for (let i = 0; i < bufferArray.length; i++) {
            let key = bufferArray[i];
            let item = this.bufferObj[key];
            let { location, size, buffer, value, target } = item;
            gl.bindBuffer(target, buffer);
            gl.vertexAttribPointer(location, size, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(location);
            gl.drawArrays(gl.POINTS, 0, value.length / size);
        }
    };
}
