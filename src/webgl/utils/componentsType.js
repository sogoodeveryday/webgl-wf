const componentType = {
    stage: "stage", //舞台
    scene: "scene", //场景
    renderer: "renderer", //渲染
    OrthographicCamera: "OrthographicCamera", //正射投影相机
    PerspectiveCamera: "PerspectiveCamera", //透视投影相机
    AmbientLight: "AmbientLight", //环境光
    DirectionalLight: "DirectionalLight", //平行灯光
    PointLight: "PointLight", //点光源
    MeshPicker: "MeshPicker",//集合体被选中
};
export default componentType;
