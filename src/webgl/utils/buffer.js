export function buffer(opentions) {
    return {
        name: "buffer",
        fn: function (value) {
            return Buffer.from(value);
        },
    };
}
