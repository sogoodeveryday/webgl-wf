// 使用示例
// const loader = new Loader();
// loader.add("image1","image1.png");
// loader.add("image2","image2.png");

// loader.onProgress((loaded, total) => {
//     console.log(`加载进度：${loaded}/${total}`);
// });

// loader.onError((name, url) => {
//     console.log(`加载失败：${name} - ${url}`);
// });

// loader.load((resources) => {
//     console.log("所有资源加载完成", resources);
// });
export class Loader {
    constructor() {
        this.resources = {};//资源泪飙
        this.queue = [];
        this.isLoading = false;
        this._instance = null;
    }
    //获取单例
    instance() {
        if (!this._instance) {
            this._instance = new Loader();
        }
        return this._instance;
    }

    add(name, url) {
        this.queue.push({ url, name });
        return this;
    }

    load(callback) {
        
        if (this.isLoading) {
            console.log("资源正在加载中，请稍后再试");
            return this;
        }

        this.isLoading = true;
        const totalResources = this.queue.length;
        let loadedResources = 0;

        this.queue.forEach((resource) => {
            const { url, name } = resource;
            const img = new Image();
            img.onload = () => { 
                this.resources[name] = img;
                loadedResources++;
                if (loadedResources === totalResources) {
                    this.isLoading = false;
                    callback(this.resources);
                }
            };
            //打开图片的跨域
            img.crossOrigin = 'anonymous';
            img.src = url;
        });
        return this;
    }

    getResource(name) {
        return this.resources[name];
    }

    reset() {
        this.resources = {};
        this.queue = [];
        this.isLoading = false;
        return this;
    }

    onProgress(callback) {
        let totalResources = this.queue.length;
        let loadedResources = 0;

        this.queue.forEach((resource) => {
            const { url, name } = resource;
            const img = new Image();
            img.onload = () => {
                loadedResources++;
                callback(loadedResources, totalResources);
            };
            img.src = url;
        });
        return this;
    }

    onError(callback) {
        this.queue.forEach((resource) => {
            const { url, name } = resource;
            const img = new Image();
            img.onerror = () => {
                callback(name, url);
            };
            img.src = url;
        });
        return this;
    }
}
