const windowEvent = {
    handleResize: (callBack) => {
        window.addEventListener("resize", () => {
            const clientWidth = document.documentElement.clientWidth;
            const clientHeight = document.documentElement.clientHeight;
            const innerWidth = document.innerWidth;
            const innerHeight = document.innerHeight;
            if (callBack)
                callBack({
                    clientWidth,
                    clientHeight,
                    innerWidth,
                    innerHeight,
                });
        });
    },
};

export { windowEvent };
