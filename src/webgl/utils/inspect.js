/**数组中有参数
 * @param {Array} array
 * @returns
 */
function arrayLength(array = []) {
    return array && array.length && array.length > 0 && array.length;
}
/**
 * 是Objact类型
 * @param {Object} obj
 * @returns
 */
function isObject(obj = {}) {
    return obj && typeof obj === "object" && obj.constructor === Object;
}
/**
 * 数组类型
 * @param {Array} arr
 * @returns
 */
function isArray(arr = []) {
    return arr && arr.constructor === Array;
}
/**
 * objact中有值
 * @param {Object} obj
 * @returns
 */
function objectHaveVlaue(obj = {}) {
    let array = Object.keys(obj);
    return array && array.length && array.length > 0 && array.length;
}
/**
 * 判断是否为字符串
 * @param {string} str
 */
function isString(str) {
    return typeof str === "string";
}
//区分是否为类型化数组
function isArrayBuffer(value) {
    return value instanceof Uint8Array || value instanceof Uint16Array || value instanceof Uint32Array || value instanceof Float32Array || value instanceof Float64Array;
}

export { arrayLength, objectHaveVlaue, isObject, isArray, isString, isArrayBuffer };
