// Define a function to convert color values
function convertColorValue(colorValue, targetFormat) {
    // Remove the '#' symbol from the color value
    colorValue = colorValue.replace("#", "");

    // Convert the color value to RGB format
    if (targetFormat === "RGB") {
        var r = parseInt(colorValue.substring(0, 2), 16);
        var g = parseInt(colorValue.substring(2, 4), 16);
        var b = parseInt(colorValue.substring(4, 6), 16);
        return [r, g, b];
    }
    // Convert the color value to HSL format
    else if (targetFormat === "HSL") {
        var r = parseInt(colorValue.substring(0, 2), 16) / 255;
        var g = parseInt(colorValue.substring(2, 4), 16) / 255;
        var b = parseInt(colorValue.substring(4, 6), 16) / 255;

        var max = Math.max(r, g, b);
        var min = Math.min(r, g, b);
        var h,
            s,
            l = (max + min) / 2;

        if (max === min) {
            h = s = 0; // Gray color
        } else {
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }

            h /= 6;
        }

        h = Math.round(h * 360);
        s = Math.round(s * 100);
        l = Math.round(l * 100);

        // return "HSL format: hsl(" + h + ", " + s + "%, " + l + "%)";
        return [h, s, l];
    } else {
        // Invalid target format
        return "Invalid target format";
    }
}

// Call the function and pass the color value and target format
// var hexColorValue = '#FF0000';
// var rgbFormat = convertColorValue(hexColorValue, 'RGB');
// var hslFormat = convertColorValue(hexColorValue, 'HSL');

// // Output the converted color values
// console.log(rgbFormat); // Output: RGB format: rgb(255, 0, 0)
// console.log(hslFormat); // Output: HSL format: hsl(0, 100%, 50%)

function HEX2RGB(hexColorValue) {
    return convertColorValue(hexColorValue, "RGB");
}
function HEX2HSL() {
    return convertColorValue(hexColorValue, "HSL");
}
export { HEX2RGB, HEX2HSL };

class ColorConverter {
    static hexToRGBA(hex) {
        // 将十六进制颜色转换为RGBA格式
        const r = parseInt(hex.substring(1, 3), 16);
        const g = parseInt(hex.substring(3, 5), 16);
        const b = parseInt(hex.substring(5, 7), 16);
        return `rgba(${r}, ${g}, ${b}, 1.0)`;
    }

    static rgbToRGBA(rgb) {
        // 将RGB颜色转换为RGBA格式
        const match = rgb.match(/(\d+),\s*(\d+),\s*(\d+)/);
        if (match) {
            return `rgba(${match[1]}, ${match[2]}, ${match[3]}, 1.0)`;
        }
        return null;
    }

    static rgbaToRGBA(rgba) {
        // 直接返回RGBA格式
        return rgba;
    }

    static hslToRGBA(hsl) {
        // 将HSL颜色转换为RGBA格式
        const match = hsl.match(/(\d+),\s*(\d+)%,\s*(\d+)%/);
        if (match) {
            const h = parseInt(match[1]) / 360;
            const s = parseInt(match[2]) / 100;
            const l = parseInt(match[3]) / 100;
            const c = (1 - Math.abs(2 * l - 1)) * s;
            const x = c * (1 - Math.abs(((h * 6) % 2) - 1));
            const m = l - c / 2;
            let r, g, b;
            if (h < 1 / 6) {
                [r, g, b] = [c, x, 0];
            } else if (h < 2 / 6) {
                [r, g, b] = [x, c, 0];
            } else if (h < 3 / 6) {
                [r, g, b] = [0, c, x];
            } else if (h < 4 / 6) {
                [r, g, b] = [0, x, c];
            } else if (h < 5 / 6) {
                [r, g, b] = [x, 0, c];
            } else {
                [r, g, b] = [c, 0, x];
            }
            r = Math.round((r + m) * 255);
            g = Math.round((g + m) * 255);
            b = Math.round((b + m) * 255);
            return `rgba(${r}, ${g}, ${b}, 1.0)`;
        }
        return null;
    }
}
function colorToRGBA(color) {
    color = color.trim().toLowerCase();
    color = replaceAll("'", '"');
    if (color.startsWith("#")) {
        // 十六进制颜色
        return ColorConverter.hexToRGBA(color);
    } else if (color.startsWith("rgb(")) {
        // RGB颜色
        return ColorConverter.rgbToRGBA(color);
    } else if (color.startsWith("rgba(")) {
        // RGBA颜色
        return ColorConverter.rgbaToRGBA(color);
    } else if (color.startsWith("hsl(")) {
        // HSL颜色
        return ColorConverter.hslToRGBA(color);
    } else {
        // 无法识别的格式，返回null或者默认颜色
        return null;
    }
}
