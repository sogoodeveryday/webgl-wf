/**
 * @namespace math
 * @type {Object}
 */
const math = {
    /**
     * 角度值转弧度值
     * @type {Number}
     */
    DEG2RAD: Math.PI / 180,
    /**
     * 弧度值转角度值
     * @type {Number}
     */
    RAD2DEG: 180 / Math.PI,
    /**
     * 生成唯一ID
     * @function
     * @param  {String} [prefix=''] ID前缀
     * @return {String} ID
     */
    generateUUID: (() => {
        let uid = 0;
        return (prefix) => {
            let id = ++uid;
            if (prefix) {
                id = prefix + "_" + id;
            } else {
                id += "";
            }
            return id;
        };
    })(),
    /**
     * 截取
     * @param  {Number} value 值
     * @param  {Number} min 最小值
     * @param  {Number} max 最大值
     * @return {Number}
     */
    clamp(value, min, max) {
        return Math.max(min, Math.min(max, value));
    },
    /**
     * 角度值转换成弧度值
     * @param  {Number} deg 角度值
     * @return {Number} 弧度值
     */
    degToRad(deg) {
        return deg * this.DEG2RAD;
    },
    /**
     * 弧度值转换成角度值
     * @param  {Number} rad 弧度值
     * @return {Number} 角度值
     */
    radToDeg(rad) {
        return rad * this.RAD2DEG;
    },
    /**
     * 是否是 2 的指数值
     * @param  {Number}  value
     * @return {Boolean}
     */
    isPowerOfTwo(value) {
        return (value & (value - 1)) === 0 && value !== 0;
    },
    /**
     * 最近的 2 的指数值
     * @param  {Number} value
     * @return {Number}
     */
    nearestPowerOfTwo(value) {
        return 2 ** Math.round(Math.log(value) / Math.LN2);
    },
    /**
     * 下一个的 2 的指数值
     * @param  {Number} value
     * @return {Number}
     */
    nextPowerOfTwo(value) {
        value--;
        value |= value >> 1;
        value |= value >> 2;
        value |= value >> 4;
        value |= value >> 8;
        value |= value >> 16;
        value++;

        return value;
    },

    /** *
     * 使用示例var cubeVertices = [    -1.0, -1.0, 1.0,    1.0, -1.0, 1.0,    1.0, 1.0, 1.0,    -1.0, 1.0, 1.0,    -1.0, -1.0, -1.0,    1.0, -1.0, -1.0,    1.0, 1.0, -1.0,    -1.0, 1.0, -1.0];var sphereVertices = [    // 球体的顶点数据    // ...];var cubeBoundingBox = calculateBoundingBox(cubeVertices);var sphereBoundingBox = calculateBoundingBox(sphereVertices);console.log(cubeBoundingBox);console.log(sphereBoundingBox);
     * 计算几何体的包围盒
     * @param {number[]} vertices - 包含几何体顶点坐标的数组
     * @param {number} size - 一个顶点包含几个数据
     * @returns {Object} 表示包围盒的对象，包含最小和最大顶点坐标
     * */
    calculateBoundingBox(vertices,size) {
        // 计算包围盒
        var minX = Math.min(...vertices.filter((v, i) => i % size === 0));
        var minY = Math.min(...vertices.filter((v, i) => i % size === 1));
        var minZ = Math.min(...vertices.filter((v, i) => i % size === 2));
        var maxX = Math.max(...vertices.filter((v, i) => i % size === 0));
        var maxY = Math.max(...vertices.filter((v, i) => i % size === 1));
        var maxZ = Math.max(...vertices.filter((v, i) => i % size === 2));
        // 创建包围盒对象
        var boundingBox = {
            min: { x: minX, y: minY, z: minZ },
            max: { x: maxX, y: maxY, z: maxZ },
        };
        return boundingBox;
    },
    /**
     * 射线拾取方法
     * @param {vec3} rayOrigin - 射线的起点
     * @param {vec3} rayDirection - 射线的方向
     * @param {Object} boundingBox - 表示包围盒的对象，包含最小和最大顶点坐标
     * @returns {boolean} 表示射线是否与包围盒相交的布尔值
     * 
     *  // 使用示例
        var rayOrigin = vec3.fromValues(0, 0, 0); // 射线起点
        var rayDirection = vec3.fromValues(0, 0, -1); // 射线方向
        
        var boundingBox = {
            min: vec3.fromValues(-1, -1, -1),
            max: vec3.fromValues(1, 1, 1)
        };
        
        var isIntersect = raycast(rayOrigin, rayDirection, boundingBox);
        console.log(isIntersect);
     */
    raycast(rayOrigin, rayDirection, boundingBox) {
        var invDirection = vec3.create();
        vec3.set(invDirection, 1.0 / rayDirection[0], 1.0 / rayDirection[1], 1.0 / rayDirection[2]);
    
        var tMin = vec3.create();
        var tMax = vec3.create();
        vec3.sub(tMin, boundingBox.min, rayOrigin);
        vec3.sub(tMax, boundingBox.max, rayOrigin);
        vec3.mul(tMin, tMin, invDirection);
        vec3.mul(tMax, tMax, invDirection);
    
        var t1 = vec3.create();
        var t2 = vec3.create();
        vec3.min(t1, tMin, tMax);
        vec3.max(t2, tMin, tMax);
    
        var tNear = Math.max(t1[0], t1[1], t1[2]);
        var tFar = Math.min(t2[0], t2[1], t2[2]);
    
        if (tNear > tFar || tFar < 0) {
            return false;
        }
    
        return true;
    }
};

export default math;
