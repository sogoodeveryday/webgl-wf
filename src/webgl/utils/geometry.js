import { math } from './index.js'

/**
 *  调用方法创建一个单位立方体
    const unitCube = createCube(1, 1, 1);
    console.log(unitCube);
 * @param {number} width 
 * @param {number} height 
 * @param {number} depth 
 * @returns {Object} objact 返回一个objact 包含相应的
 * @returns {Array} [objact.vertices] 顶点坐标
 * @returns {Array} [objact.normal]  面的法向量
 * @returns {Array} [objact.color] 颜色值
 * @returns {Array} [objact.vertices]返回一个objact 包含相应的
 */
function createCube(width, height, depth) {
    // const verticesInfo = [
    //     // 定义立方体的顶点坐标
    //     // 前面四个顶点
    //     { position: [-width / 2, -height / 2, depth / 2], color: [1.0, 1.0, 1.0], normal: [0, 0, 1] },
    //     { position: [width / 2, -height / 2, depth / 2], color: [1.0, 0.0, 1.0], normal: [0, 0, 1] },
    //     { position: [width / 2, height / 2, depth / 2], color: [1.0, 0.0, 0.0], normal: [0, 0, 1] },
    //     { position: [-width / 2, height / 2, depth / 2], color: [1.0, 1.0, 0.0], normal: [0, 0, 1] },
    //     // 后面四个顶点
    //     { position: [-width / 2, -height / 2, -depth / 2], color: [0.0, 1.0, 0.0], normal: [0, 0, -1] },
    //     { position: [width / 2, -height / 2, -depth / 2], color: [0.0, 1.0, 1.0], normal: [0, 0, -1] },
    //     { position: [width / 2, height / 2, -depth / 2], color: [0.0, 0.0, 1.0], normal: [0, 0, -1] },
    //     { position: [-width / 2, height / 2, -depth / 2], color: [0.0, 0.0, 0.0], normal: [0, 0, -1] },
    // ];
    // const indicesInfo = [
    //     // 定义立方体的面的索引
    //     [0, 1, 2, 0, 2, 3], // 前面
    //     [4, 5, 6, 4, 6, 7], // 后面
    //     [1, 5, 6, 1, 6, 2], // 右侧
    //     [0, 4, 7, 0, 7, 3], // 左侧
    //     [3, 2, 6, 3, 6, 7], // 上面
    //     [0, 1, 5, 0, 5, 4], // 下面
    // ];

    // //顶点索引数据
    // const indices = [];
    // //顶点的坐标
    // const vertices = [];
    // //法向量
    // const normals = [];
    // //顶点的颜色
    // const colors = [];
    // for (let i = 0; i < verticesInfo.length; i++) {
    //     let item = verticesInfo[i];
    //     vertices.push(...item.position);
    //     normals.push(...item.normal);
    //     colors.push(...item.color);
    // }
    // for (let i = 0; i < indicesInfo.length; i++) {
    //     let item = indicesInfo[i];
    //     indices.push(...item);
    // }
    // var verticesBase = [
    //     1.0, 1.0, 1.0,   // v0 White
    //     -1.0, 1.0, 1.0,  // v1 Magenta
    //     -1.0, -1.0, 1.0, // v2 Red
    //     1.0, -1.0, 1.0,  // v3 Yellow
    //     1.0, -1.0, -1.0, // v4 Green
    //     1.0, 1.0, -1.0,  // v5 Cyan
    //     -1.0, 1.0, -1.0,  // v6 Blue
    //     -1.0, -1.0, -1.0, // v7 Black
    // ];
    // let vertices = verticesBase.map((item)=>{
    //     return item/2
    // })

    // //颜色
    // var colors = [
    //     1.0, 1.0, 1.0,  // v0 White
    //     1.0, 0.0, 1.0,  //v1 Magenta
    //     1.0, 0.0, 0.0,  // v2 Red
    //     1.0, 1.0, 0.0,  // v3 Yellow
    //     0.0, 1.0, 0.0,  // v4 Green
    //     0.0, 1.0, 1.0,  // v5 Cyan
    //     0.0, 0.0, 1.0,  // v6 Blue
    //     0.0, 0.0, 0.0   // v7 Black
    // ]
    // // indx
    // var indices = new Uint8Array([
    //     1, 6, 7, 1, 7, 2,    // left
    //     7, 4, 3, 7, 3, 2,    // down
    //     4, 7, 6, 4, 6, 5,    // back
    //     0, 1, 2, 0, 2, 3,    // front
    //     0, 3, 4, 0, 4, 5,    // right
    //     0, 5, 6, 0, 6, 1,    // up
    // ]);
    // 需要顶点个数18个
    // Create a cube
    //    v6----- v5
    //   /|      /|
    //  v1------v0|
    //  | |     | |
    //  | |v7---|-|v4
    //  |/      |/
    //  v2------v3
    // var verticesBase =[
    //     1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, // v0-v1-v2-v3 front
    //     1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, // v0-v3-v4-v5 right
    //     1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, // v0-v5-v6-v1 up
    //     -1.0, 1.0, 1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, // v1-v6-v7-v2 left
    //     -1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, -1.0, 1.0, -1.0, -1.0, 1.0, // v7-v4-v3-v2 down
    //     1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, 1.0, 1.0, -1.0  // v4-v7-v6-v5 back
    // ];

    var verticesBase = [
        width , height , depth , -width , height , depth , -width , -height , depth , width , -height , depth , // v0-v1-v2-v3 front
        width , height , depth , width , -height , depth , width , -height , -depth , width , height , -depth , // v0-v3-v4-v5 right
        width , height , depth , width , height , -depth , -width , height , -depth , -width , height , depth , // v0-v5-v6-v1 up
        -width , height , depth , -width , height , -depth , -width , -height , -depth , -width , -height , depth , // v1-v6-v7-v2 left
        -width , -height , -depth , width , -height , -depth , width , -height , depth , -width , -height , depth , // v7-v4-v3-v2 down
        width , -height , -depth , -width , -height , -depth , -width , height , -depth , width , height , -depth   // v4-v7-v6-v5 back
    ]; 
    //纹理坐标
    var texCoords = ([   // Texture coordinates
        1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,    // v0-v1-v2-v3 front
        0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0,    // v0-v3-v4-v5 right
        1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0,    // v0-v5-v6-v1 up
        1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,    // v1-v6-v7-v2 left
        0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,    // v7-v4-v3-v2 down
        0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0     // v4-v7-v6-v5 back
    ]);
    texCoords  = texCoords.map((item)=>{
        return item/2-1;
    })

    let vertices = verticesBase.map((item) => {
        return item / 1;
    });
    //颜色
    var colors = ([     // Colors
        0.4, 0.4, 1.0, 0.4, 0.4, 1.0, 0.4, 0.4, 1.0, 0.4, 0.4, 1.0,  // v0-v1-v2-v3 front(blue)
        0.4, 1.0, 0.4, 0.4, 1.0, 0.4, 0.4, 1.0, 0.4, 0.4, 1.0, 0.4,  // v0-v3-v4-v5 right(green)
        1.0, 0.4, 0.4, 1.0, 0.4, 0.4, 1.0, 0.4, 0.4, 1.0, 0.4, 0.4,  // v0-v5-v6-v1 up(red)
        1.0, 1.0, 0.4, 1.0, 1.0, 0.4, 1.0, 1.0, 0.4, 1.0, 1.0, 0.4,  // v1-v6-v7-v2 left
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,  // v7-v4-v3-v2 down
        0.4, 1.0, 1.0, 0.4, 1.0, 1.0, 0.4, 1.0, 1.0, 0.4, 1.0, 1.0   // v4-v7-v6-v5 back
    ]);
    var colors2 =[    // Colors
        1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,     // v0-v1-v2-v3 front
        1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,     // v0-v3-v4-v5 right
        1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,     // v0-v5-v6-v1 up
        1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,     // v1-v6-v7-v2 left
        1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,     // v7-v4-v3-v2 down
        1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0　    // v4-v7-v6-v5 back
    ];
    var colors3 =[    // Colors
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     // v0-v1-v2-v3 front
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     // v0-v3-v4-v5 right
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     // v0-v5-v6-v1 up
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     // v1-v6-v7-v2 left
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,     // v7-v4-v3-v2 down
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,　    // v4-v7-v6-v5 back
    ];
    // indx
    var indices = new Uint8Array([
        // Indices of the vertices
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12,
        14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23,
    ]);
    //法向量
    var normals =[    // Normal
        0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0,  // v0-v1-v2-v3 front
        1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,  // v0-v3-v4-v5 right
        0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,  // v0-v5-v6-v1 up
        -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0,  // v1-v6-v7-v2 left
        0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0,  // v7-v4-v3-v2 down
        0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0   // v4-v7-v6-v5 back
    ];
    let boundingBox = math.calculateBoundingBox(vertices,3)
    return {
        vertices: {
            value: vertices,
            size: 3,
        },
        normals: {
            value: normals,
            size: 3,
        },
        colors: {
            value: colors,
            size: 3,
        },
        colors2: {
            value: colors2,
            size: 3,
        },
        colors3:{
            value: colors3,
            size: 3,
        },
        texCoords:{
            value: texCoords,
            size: 2,
        },
        indices,
        boundingBox,
        // verticesInfo,
    };
}

/**
 * 
    创建球体的几何体
    const sphereGeometry = createSphere(1, 20, 20);
    console.log(createSphere);
 * @param {number} radius  半径
 * @param {number} widthSegments  宽度
 * @param {number} heightSegments 高度
 * @returns 
 */
function createSphere(radius, widthSegments, heightSegments) {
    const color = [1, 0, 0, 1]; // 默认红色
    const vertices = [];
    const indices = [];
    const normals = [];
    const colors = [];
    const position = [];

    // 生成球体的顶点坐标
    for (let i = 0; i <= heightSegments; i++) {
        const theta = (i * Math.PI) / heightSegments;
        const sinTheta = Math.sin(theta);
        const cosTheta = Math.cos(theta);

        for (let j = 0; j <= widthSegments; j++) {
            const phi = (j * 2 * Math.PI) / widthSegments;
            const sinPhi = Math.sin(phi);
            const cosPhi = Math.cos(phi);

            const x = radius * cosPhi * sinTheta;
            const y = radius * cosTheta;
            const z = radius * sinPhi * sinTheta;
            vertices.push(...[x, y, z]);
            colors.push(...color);
            normals.push(...[x, y, z]);

            // vertices.push({ position: [x, y, z], color: color, normal: [x, y, z] });
        }
    }

    // 生成球体的面的索引
    for (let i = 0; i < heightSegments; i++) {
        for (let j = 0; j < widthSegments; j++) {
            const first = i * (widthSegments + 1) + j;
            const second = first + widthSegments + 1;
            indices.push([first, second, first + 1]);
            indices.push([second, second + 1, first + 1]);
        }
    }

    return {
        indices,
        vertices: {
            value: vertices,
            size: 3,
        },
        normals: {
            value: normals,
            size: 3,
        },
        colors: {
            value: colors,
            size: 4,
        },
    };
}

/**
 * 
    创建胶囊体的几何体
    const capsuleGeometry = createCapsule(1, 2, 20, 20);
    console.log(capsuleGeometry);
    这个示例中，我们定义了一个名为createCapsule的函数，
    该函数接受胶囊体的半径、高度、径向分段数和高度分段数作为参数，并返回一个包含胶囊体顶点和面索引的对象。
 * @param {number} radius 半径
 * @param {number} height 高度
 * @param {number} radialSegments 径向分段数
 * @param {number} heightSegments 高度分段数
 * @returns 
 */
function createCapsule(radius, height, radialSegments, heightSegments) {
    const color = [1, 1, 1, 1]; // 默认白色
    const vertices = [];
    const indices = [];

    // 生成胶囊体的顶点坐标
    for (let i = 0; i <= heightSegments; i++) {
        const y = (i / heightSegments) * height - height / 2;
        const v = i / heightSegments;
        const radiusAtHeight = Math.sqrt(
            radius * radius - (y + height / 2) * (y + height / 2)
        );

        for (let j = 0; j <= radialSegments; j++) {
            const u = j / radialSegments;
            const theta = u * Math.PI * 2;
            const x = radiusAtHeight * Math.cos(theta);
            const z = radiusAtHeight * Math.sin(theta);

            vertices.push({
                position: [x, y, z],
                color: color,
                normal: [x, y, z],
            });
        }
    }

    // 生成胶囊体的面的索引
    for (let i = 0; i < heightSegments; i++) {
        for (let j = 0; j < radialSegments; j++) {
            const a = i * (radialSegments + 1) + j;
            const b = i * (radialSegments + 1) + j + 1;
            const c = (i + 1) * (radialSegments + 1) + j;
            const d = (i + 1) * (radialSegments + 1) + j + 1;

            indices.push([a, b, d]);
            indices.push([a, d, c]);
        }
    }

    return {
        vertices: vertices,
        indices: indices,
    };
}

/**
 
    创建圆柱体的几何体
    const createCylinder = createCylinder(1, 1, 2, 20, 20);
    console.log(createCylinder);
 * @param {*} radiusTop 
 * @param {*} radiusBottom 
 * @param {*} height 
 * @param {*} radialSegments 
 * @param {*} heightSegments 
 * @returns 
 */
function createCylinder(
    radiusTop,
    radiusBottom,
    height,
    radialSegments,
    heightSegments
) {
    const color = [1, 1, 1, 1]; // 默认白色
    const vertices = [];
    const indices = [];

    // 生成圆柱体的顶点坐标
    for (let i = 0; i <= heightSegments; i++) {
        const y = (i / heightSegments) * height - height / 2;
        const v = i / heightSegments;

        for (let j = 0; j <= radialSegments; j++) {
            const u = j / radialSegments;
            const theta = u * Math.PI * 2;
            const x = Math.cos(theta);
            const z = Math.sin(theta);
            const radius = radiusBottom + (radiusTop - radiusBottom) * v;

            vertices.push({
                position: [x * radius, y, z * radius],
                color: color,
                normal: [x, 0, z],
            });
        }
    }

    // 生成圆柱体的面的索引
    for (let i = 0; i < heightSegments; i++) {
        for (let j = 0; j < radialSegments; j++) {
            const a = i * (radialSegments + 1) + j;
            const b = i * (radialSegments + 1) + j + 1;
            const c = (i + 1) * (radialSegments + 1) + j;
            const d = (i + 1) * (radialSegments + 1) + j + 1;

            indices.push([a, b, d]);
            indices.push([a, d, c]);
        }
    }

    return {
        vertices: vertices,
        indices: indices,
    };
}

export { createCube, createSphere, createCapsule, createCylinder };
