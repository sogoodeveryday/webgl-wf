/**js中使用webgl中向glsl中传递方法的枚举 */
let uniform2JsTypeEnum = {
    float: "uniform1f",
    int: "uniform1i",
    bool: "uniform1i", // 实际上bool会被转换为int
    vec2: "uniform2f",
    vec3: "uniform3f",
    vec4: "uniform4f",
    mat2: "uniformMatrix2fv",
    mat3: "uniformMatrix3fv",
    mat4: "uniformMatrix4fv",
};
/**
 *glsl中unitfrom的赋值方法
 @param {WebGLRenderingContext} gl webgl的上下文
 @param {WebGLUniformLocation} location webgl中uniform表变量地址
 @param {Float32List || Int32List || Uint32List} value 传递的值
 */
const glsl2ArraySetters = { 
    float: (gl, location, value) => {
        gl.uniform1f(location, value);
    },
    vec2: (gl, location, value) => {
        gl.uniform2fv(location, value);
    },
    vec3: (gl, location, value) => {
        gl.uniform3fv(location, value);
    },
    vec4: (gl, location, value) => {
        gl.uniform4fv(location, value);
    },
    int: (gl, location, value) => {
        gl.uniform1iv(location, value);
    },
    ivec2: (gl, location, value) => {
        gl.uniform2iv(location, value);
    },
    ivec3: (gl, location, value) => {
        gl.uniform3iv(location, value);
    },
    ivec4: (gl, location, value) => {
        gl.uniform4iv(location, value);
    },
    uint: (gl, location, value) => {
        gl.uniform1ui(location, value);
    },
    uvec2: (gl, location, value) => {
        gl.uniform2uiv(location, value);
    },
    uvec3: (gl, location, value) => {
        gl.uniform3uiv(location, value);
    },
    uvec4: (gl, location, value) => {
        gl.uniform4uiv(location, value);
    },
    bool: (gl, location, value) => {
        gl.uniform1i(location, value);
    },
    bvec: (gl, location, value) => {
        gl.uniform2iv(location, value);
    },
    bvec2: (gl, location, value) => {
        gl.uniform3iv(location, value);
    },
    bvec3: (gl, location, value) => {
        gl.uniform4iv(location, value);
    },
    mat2: (gl, location, value) => {
        gl.uniformMatrix2fv(location, false, value);
    },
    mat3: (gl, location, value) => {
        gl.uniformMatrix3fv(location, false, value);
    },
    mat4: (gl, location, value) => {
        gl.uniformMatrix4fv(location, false, value);
    },
    sampler2D: (gl, location, value) => {
        gl.uniform1iv(location, value);
    },
    samplerCube: (gl, location, value) => {
        gl.uniform1iv(location, value);
    },
    sampler2DArray: (gl, location, value) => {
        gl.uniform1iv(location, value);
    },
};

export { uniform2JsTypeEnum, glsl2ArraySetters };
