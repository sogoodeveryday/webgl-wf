import { BaseCamera } from "./index";
import { mat4 } from "gl-matrix";
import { componentType, math, windowEvent } from "@webgl/utils/index.js";

// PerspectiveCamera 类继承自 BaseCamera 类
export default class PerspectiveCamera extends BaseCamera {
    constructor(fov, aspect, near, far) {
        super(); // 调用父类的构造函数
        this.className = componentType.PerspectiveCamera; // 设置类名
        this.uuid = math.generateUUID(this.calssName); // 生成唯一标识符
        this.fov = fov || 75; // 视场角度，默认为 75 度
        this.aspect = aspect || window.innerWidth / window.innerHeight; // 宽高比，默认为当前窗口的宽高比
        this.near = near || 0.1; // 近裁剪面，默认为 0.1
        this.far = far || 1000; // 远裁剪面，默认为 1000
        this.projMatrix = mat4.create(); // 创建正射投影矩阵
        this.projMatrixKey = "u_ProjMatrix"; // 投影矩阵的 uniform 变量名
        this.setDate(fov, aspect, near, far); // 设置投影矩阵
    }

    // 设置投影矩阵
    setDate(fov, aspect, near, far) {
        mat4.perspective(this.projMatrix, fov, aspect, near, far);
    }
}

// //透视相机（Perspective Camera）：透视相机是一种模拟人眼视觉的相机类型，
// // 它可以创建透视效果，使远处的物体看起来比近处的物体小。
// // 透视相机通常用于创建逼真的3D场景。
// import { BaseCamera } from "./index";
// import { mat4 } from "gl-matrix";
// import { componentType, math, windowEvent } from "@webgl/utils/index.js";
// export default class PerspectiveCamera extends BaseCamera {
//     constructor(fov, aspect, near, far) {
//         super();
//         this.className = componentType.PerspectiveCamera;
//         this.uuid = math.generateUUID(this.calssName);
//         this.fov = fov || 75; // 视场角度
//         this.aspect = aspect || window.innerWidth / window.innerHeight; // 宽高比
//         this.near = near || 0.1; // 近裁剪面
//         this.far = far || 1000; // 远裁剪面
//         this.projMatrix = mat4.create(); //正射投影矩阵
//         this.projMatrixKey = "u_ProjMatrix"; //投影矩阵的key值
//         this.setDate(fov, aspect, near, far);
//     }
//     setDate(fov, aspect, near, far) {
//         mat4.perspective(this.projMatrix, fov, aspect, near, far);
//     }
// }
//透视相机（Perspective Camera）：透视相机是一种模拟人眼视觉的相机类型，
// 它可以创建透视效果，使远处的物体看起来比近处的物体小。
// 透视相机通常用于创建逼真的3D场景。
