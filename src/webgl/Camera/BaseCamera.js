import { mat4 } from "gl-matrix";

// BaseCamera 类是一个基础相机类，提供了一些基本的属性和方法
export default class BaseCamera {
    constructor() {
        // 视点位置
        this._position = [1, 1, 15];
        // 观察目标位置
        this._lookAt = [0, 0, 0];
        // 上方向
        this._up = [0, 1, 0];
        // 视图矩阵
        this.viewMatrix = mat4.create();
        // 视图矩阵的 uniform 变量名
        this.viewMatrixKey = "u_viewMatrix";
    }

    // 设置视点位置
    set position(value) {
        this._position = value;
        this.updateViewMatrix();
    }

    get position() {
        return this._position;
    }

    // 设置观察目标位置
    set lookAt(value) {
        this._lookAt = value;
        this.updateViewMatrix();
    }

    get lookAt() {
        return this._lookAt;
    }

    // 设置上方向
    set up(value) {
        this._up = value;
        this.updateViewMatrix();
    }

    get up() {
        return this._up;
    }

    // 更新视图矩阵
    updateViewMatrix = () => {
        mat4.lookAt(this.viewMatrix, this._position, this._lookAt, this._up);
    };

    // 更新相机参数
    update = (mesh) => {
        // 将视图矩阵和投影矩阵设置到 mesh 的材质中
        mesh?.material.setUniformFormat({
            [this.projMatrixKey]: {
                value: this.projMatrix,
            },
            [this.viewMatrixKey]: {
                value: this.viewMatrix,
            },
        });
    };

    // 销毁相机
    destroy() {}
}
// import { mat4 } from "gl-matrix";
// export default class BaseCamera {
//     constructor() {
//         //视点
//         this._position = [1, 1, 15];
//         this._lookAt = [0, 0, 0];
//         this._up = [0, 1, 0];
//         this.viewMatrix = mat4.create();
//         this.viewMatrixKey = "u_viewMatrix"; //视图矩阵key值
//     }
//     set position(value) {
//         this._position = value;

//         // console.log(value);
//         this.updateViewMatrix();
//     }
//     get position() {
//         return this._position;
//     }
//     set lookAt(value) {
//         this._lookAt = value;
//         this.updateViewMatrix();
//     }
//     get lookAt() {
//         return this._lookAt;
//     }
//     set up(value) {
//         this._up = value;
//         this.updateViewMatrix();
//     }
//     get up() {
//         return this._up;
//     }
//     updateViewMatrix = () => {
//         // 更新视图矩阵
//         mat4.lookAt(this.viewMatrix, this._position, this._lookAt, this._up);
//     };
//     // 更新相机
//     // update() {}
//     update = (mesh) => {
//         //获取矩阵的相关key 然后赋值
//         mesh?.material.setUniformFormat({
//             [this.projMatrixKey]: {
//                 value: this.projMatrix,
//             },
//             [this.viewMatrixKey]: {
//                 value: this.viewMatrix,
//             },
//         });
//     };
//     // 销毁相机
//     destroy() {}
// }
