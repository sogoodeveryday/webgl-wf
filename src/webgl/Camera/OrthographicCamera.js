import { mat4 } from "gl-matrix";
import { BaseCamera } from "./index.js";
import { componentType, math, windowEvent } from "@webgl/utils/index.js";

// OrthographicCamera 类继承自 BaseCamera 类
export default class OrthographicCamera extends BaseCamera {
    constructor(left, right, bottom, top, near, far) {
        super(); // 调用父类的构造函数
        this.className = componentType.OrthographicCamera; // 设置类名
        this.uuid = math.generateUUID(this.calssName); // 生成唯一标识符
        this.projMatrix = mat4.create(); // 创建正射投影矩阵
        this.projMatrixKey = "u_ProjMatrix"; // 投影矩阵的 uniform 变量名
        this.left = left || 1; // 左边界，默认为 1
        this.right = right || -1; // 右边界，默认为 -1
        this.bottom = bottom || -1; // 下边界，默认为 -1
        this.top = top || 1; // 上边界，默认为 1
        this.near = near || 0.1; // 近裁剪面，默认为 0.1
        this.far = far || 100; // 远裁剪面，默认为 100
        this.setDate(left, right, bottom, top, near, far); // 设置投影矩阵
    }

    // 设置投影矩阵
    setDate(left, right, bottom, top, near, far) {
        mat4.ortho(this.projMatrix, left, right, bottom, top, near, far);
    }
}

// // 正交相机（Orthographic Camera）：
// // 正交相机是一种不考虑远近的相机类型，
// // 它不会产生透视效果，远近的物体大小看起来是一样的。
// // 正交相机通常用于创建技术图形、CAD软件等需要精确测量的场景。
// import { mat4 } from "gl-matrix";
// import { BaseCamera } from "./index.js";
// import { componentType, math, windowEvent } from "@webgl/utils/index.js";
// export default class OrthographicCamera extends BaseCamera {
//     constructor(left, right, bottom, top, near, far) {
//         super();
//         this.className = componentType.OrthographicCamera;
//         this.uuid = math.generateUUID(this.calssName);
//         this.projMatrix = mat4.create(); //正射投影矩阵
//         this.projMatrixKey = "u_ProjMatrix"; //投影矩阵的key值
//         this.left = left || 1;
//         this.right = right || -1;
//         this.bottom = bottom || -1;
//         this.top = top || 1;
//         this.near = near || 0.1;
//         this.far = far || 100;
//         this.setDate(left, right, bottom, top, near, far);
//     }
//     setDate(left, right, bottom, top, near, far) {
//         mat4.ortho(this.projMatrix, left, right, bottom, top, near, far);
//     }
// }

// 正交相机（Orthographic Camera）：
// 正交相机是一种不考虑远近的相机类型，
// 它不会产生透视效果，远近的物体大小看起来是一样的。
// 正交相机通常用于创建技术图形、CAD软件等需要精确测量的场景。
