// varying vec2 vTextureCoord;

// uniform sampler2D uSampler;

// void main(void){
//    gl_FragColor *= texture2D(uSampler, vTextureCoord);
// }
precision highp float;
varying vec2 v_TexCoord;
uniform sampler2D u_Sampler;//纹理
void main() {
   gl_FragColor =  texture2D(u_Sampler, v_TexCoord);//纹理坐标系下的坐标
}