/**
 * 在WebGL中，Program组件通常用于管理着色器程序（Shader Program），其功能包括：

着色器加载和编译：Program组件负责加载顶点着色器和片元着色器的源代码，并进行编译。

链接着色器：将顶点着色器和片元着色器链接在一起，形成一个完整的着色器程序。

向着色器传递数据：Program组件可以用于向着色器程序传递顶点数据、纹理坐标、投影矩阵等必要的数据。

激活和使用：一旦着色器程序链接完成，Program组件可以激活并使用该着色器程序进行渲染操作。

错误处理：Program组件通常也包含了对着色器编译和链接过程中可能出现的错误进行处理和报告的功能。

通过Program组件，可以方便地管理着色器程序的创建、编译、链接和使用，从而实现复杂的图形渲染效果。
 */
import defaultFragment from "./defaultProgram.frag?raw";
import defaultVertex from "./defaultProgram.vert?raw";
import { getAttributeData, getUniformData, logProgramError } from "./utils/index";
/**
 * Program
 * @class
 * @classdesc 创建一个Program
 */
export default class Program {
    /**
     *@constructor
     * @type {config} config
     * @param {WebGL2RenderingContext} config.gl weblg的上下文
     * @param {string} config.vertexShaderSource 顶点着色器
     * @param {string} config.fragmentShaderSource 片元着色器
     * @param {shaderParameter} config.shaderParameter 着色器中的变量对应的参数
     * @type {shaderParameter} shaderParameter 往shader中需要传递的参数
     * @param {[Object]} config.attributeData [key:value]
     * @param {[Object]} config.uniformData [key:value]
     */
    constructor(config) {
        // this.attributeData = {};
        // this.uniformData = {};
        // this.allData = {};
        this.init(config);
    }

    /**
     * @function
     * @type {config} config
     * @param {WebGL2RenderingContext} config.gl weblg的上下文
     * @param {string} config.vertexShaderSource 顶点着色器
     * @param {string} config.fragmentShaderSource 片元着色器
     * @param {shaderParameter} config.shaderParameter 着色器中的变量对应的参数
     * @type {shaderParameter} shaderParameter 往shader中需要传递的参数
     * @param {[Object]} config.attributeData [key:value]
     * @param {[Object]} config.uniformData [key:value]
     */
    init(config) {
        let { gl, vertexShaderSource, fragmentShaderSource, shaderParameter } = config;
        this.gl = gl;
        this.vertexShaderSource = this.setVertexShaderSource(vertexShaderSource); // 顶点着色器源码
        this.fragmentShaderSource = this.setFragmentShaderSource(fragmentShaderSource); // 片段着色器源码
        //编辑shader
        this.compileVertShader = this.compileShader(gl, gl.VERTEX_SHADER, this.vertexShaderSource);
        this.compileFragShader = this.compileShader(gl, gl.FRAGMENT_SHADER, this.fragmentShaderSource);
        //创建程序
        let program = (this.programData = this.createLinkProgram(gl, this.compileVertShader, this.compileFragShader));
        //检测shader的编辑结果
        logProgramError(gl, program, this.compileVertShader, this.compileFragShader);
        program.attributeData = getAttributeData(gl, program);
        program.uniformData = getUniformData(gl, program);
        // program.allData = Object.assign(program.attributeData, program.uniformData);
        console.log("program:", program);
        //删除程序
        this.deleteShader(gl, program);
    }
    useProgram = () => {
        this.gl.useProgram(this.programData);
    };
    /**
     *
     * 实现创建着色器的逻辑
     * @function
     * @param {VERTEX_SHADER || FRAGMENT_SHADER} type
     * @param {string} source
     * @returns {compileShader}
     */
    compileShader(gl, type, source) {
        //创建着色器
        let shader = gl.createShader(type);
        //给着色器赋值
        gl.shaderSource(shader, source);
        //编译着色器
        gl.compileShader(shader);
        return shader;
    }
    /**
     * 创建glProgram 并赋值
     * 原生createProgram attachShader
     * @function
     * @param {compileShader} vertShader
     * @param {compileShader} fragShader
     * @returns {program}
     */
    createLinkProgram(gl, vertShader, fragShader) {
        const webGLProgram = gl.createProgram();
        gl.attachShader(webGLProgram, vertShader);
        gl.attachShader(webGLProgram, fragShader);
        gl.linkProgram(webGLProgram);
        return webGLProgram;
    }
    /**
     * 删除compileShader
     * 原生deleteShader
     * @function
     * @param {compileShader} vertShader
     * @param {compileShader} fragShader
     */
    deleteShader(gl) {
        gl.deleteShader(this.compileVertShader);
        gl.deleteShader(this.compileFragShader);
    }
    /**
     * 去掉开头结尾的空格
     * @function
     * @param {string} string
     * @returns
     */
    trim(string) {
        return string.trim();
    }
    /**
     *  顶点着色器源码
     * @function
     * @param {string} vertexShaderSource
     */
    setVertexShaderSource(vertexShaderSource = null) {
        this.vertexShaderSource = this.trim(vertexShaderSource || defaultVertex);
        return this.vertexShaderSource;
    }
    /**
     * 设置片元着色器的素材
     * @function
     * @param {string} fragmentShaderSource
     */
    setFragmentShaderSource(fragmentShaderSource = null) {
        this.fragmentShaderSource = this.trim(fragmentShaderSource || defaultFragment);
        return this.fragmentShaderSource;
    }
}

// let UID = 0;
// export default class Program {
//     constructor(confog) {
//         let { gl, vertexShaderSource, fragmentShaderSource, program = null } = confog;
//         this.gl = gl;
//         this.vertexShaderSource = this.setVertexShaderSource(vertexShaderSource); // 顶点着色器源码
//         this.fragmentShaderSource = this.setFragmentShaderSource(fragmentShaderSource); // 片段着色器源码
//         this.program = program; // WebGL 着色器程序
//     }
//     /**
//      * 去掉开头结尾的空格
//      * @param {string} string
//      * @returns
//      */
//     trim(string) {
//         return string.trim();
//     }
//     /**
//      *  顶点着色器源码
//      * @param {string} vertexShaderSource
//      */
//     setVertexShaderSource(vertexShaderSource = null) {
//         this.vertexShaderSource = this.trim(vertexShaderSource || defaultVertex);
//         return vertexShaderSource;
//     }
//     /**
//      * 设置片元着色器的素材
//      * @param {string} fragmentShaderSource
//      */
//     setFragmentShaderSource(fragmentShaderSource = null) {
//         this.fragmentShaderSource = this.trim(fragmentShaderSource || defaultFragment);
//         return fragmentShaderSource;
//     }
//     /**
//      * 获取编译后的着色器
//      * @returns program
//      */
//     getProgram() {
//         return this.program;
//     }
//     // 其他着色器属性和方法
// }
