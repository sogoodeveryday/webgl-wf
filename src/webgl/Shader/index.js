/**
 * 初始化着色器：在构造函数中初始化着色器类型（顶点着色器或片元着色器）和着色器源码。

    编译着色器：提供方法用于编译着色器源码，并检查编译错误。

    获取着色器对象：提供方法用于获取编译后的着色器对象。

    Shader组件定义了渲染的算法和逻辑，控制着物体的外观和表现方式，例如光照、阴影、颜色变换等。
 */
import Program from "./Program.js";

class Shader {
    constructor(program) {
        this.program = program;
    }
}

export { Program, Shader };
