import { getAttributeData } from "./getAttributeData";
import { getUniformData } from "./getUniformData";
export default function generateProgram(config) {
    let { gl, program } = config;
    const vertShader = compileShader(gl, gl.VERTEX_SHADER, program.vertexSrc);
    const fragShader = compileShader(gl, gl.FRAGMENT_SHADER, program.fragmentSrc);
    const webglProgram = createLinkProgram(vertShader, fragShader);

    program.attributeData = getAttributeData({ gl, webglProgram });
    program.uniformData = getUniformData({ gl, webglProgram });

    deleteShader(webglProgram);

    /**
     * 实现创建着色器的逻辑
     * @param {VERTEX_SHADER || FRAGMENT_SHADER} type
     * @param {string} source
     * @returns {compileShader}
     */
    function compileShader(type, source) {
        //创建着色器
        let shader = gl.createShader(type);
        //给着色器赋值
        gl.shaderSource(shader, source);
        //编译着色器
        gl.compileShader(shader);
        return shader;
    }
    /**
     * 创建glProgram 并赋值
     * 原生createProgram attachShader
     * @param {compileShader} vertShader
     * @param {compileShader} fragShader
     * @returns {program}
     */
    function createLinkProgram(vertShader, fragShader) {
        const webGLProgram = gl.createProgram();
        gl.attachShader(webGLProgram, vertShader);
        gl.attachShader(webGLProgram, fragShader);
        gl.linkProgram(webGLProgram);
        return webGLProgram;
    }
    /**
     * 删除compileShader
     * 原生deleteShader
     * @param {compileShader} vertShader
     * @param {compileShader} fragShader
     */
    function deleteShader(vertShader, fragShader) {
        gl.deleteShader(webGLProgram, vertShader);
        gl.deleteShader(webGLProgram, fragShader);
    }
    return program;
}
