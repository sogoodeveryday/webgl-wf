import { mapSize } from "./mapSize";
import { mapType } from "./mapType";

/**
 * 返回program程序中的attribute数据
 * @private
 * @param gl - the WebGL context
 * @param program - the webgl program
 * @returns {object}返回program程序中的attribute数据
 */
export function getAttributeData(gl, program) {
    const attributes = {};
    const totalAttributes = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
    console.log('totalAttributes:', totalAttributes)
    for (let i = 0; i < totalAttributes; i++) {
        const attribData = gl.getActiveAttrib(program, i);
        console.log('attribData:', attribData)
        if (attribData.name.startsWith("gl_")) {
            continue;
        }
        const type = mapType(gl, attribData.type);
        const data = {
            valueType: type,
            name: attribData.name,
            size: mapSize(type),
            location: gl.getAttribLocation(program, attribData.name),
        };
        attributes[attribData.name] = data;
    }
    return attributes;
}
