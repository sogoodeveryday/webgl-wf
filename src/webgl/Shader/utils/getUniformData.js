import { defaultValue } from "./defaultValue";
import { mapType } from "./mapType";
import { glsl2ArraySetters } from "@webgl/utils/index.js";

/**
 * 返回program程序中的uniform数据
 * @private
 * @param gl - the WebGL context
 * @param program - the webgl program
 * @returns {object} 返回program程序中的uniform数据
 */
export function getUniformData(gl, program) {
    const uniforms = {};

    const totalUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);

    for (let i = 0; i < totalUniforms; i++) {
        const uniformData = gl.getActiveUniform(program, i);
        const name = uniformData.name.replace(/\[.*?\]$/, "");

        const isArray = !!uniformData.name.match(/\[.*?\]$/);

        const type = mapType(gl, uniformData.type);

        uniforms[name] = {
            name,
            index: i,
            valueType: type,
            size: uniformData.size,
            isArray,
            value: defaultValue(type, uniformData.size),
            location: gl.getUniformLocation(program, uniformData.name),
            setter: glsl2ArraySetters[type],
        };
    }

    return uniforms;
}

// function createUniformSetter(gl, program, uniformInfo) {
//     let uniformLocation = gl.getUniformLocation(program, uniformInfo.name);
//     let type = uniformInfo.type;
//     let isArray = uniformInfo.size > 1 && uniformInfo.name.substr(-3) === "[0]";

//     if (isArray && type == enums.INT.value) {
//         return function (v) {
//             gl.uniform1iv(location, v);
//         };
//     }
//     if (isArray && type == enums.FLOAT.value) {
//         return function (v) {
//             gl.uniform1fv(location, v);
//         };
//     }
//     return function createSetter(v) {
//         return enums[getKeyFromType(type)].setter(location, v);
//     };
// }
