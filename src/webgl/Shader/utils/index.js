import { defaultValue } from "./defaultValue.js";
import { getAttributeData } from "./getAttributeData.js";
import { getUniformData } from "./getUniformData.js";
import { mapSize } from "./mapSize.js";
import { mapType } from "./mapType.js";
import { logProgramError } from "./logProgramError.js";

export { defaultValue, getAttributeData, getUniformData, mapSize, mapType, logProgramError };
