// attribute vec2 aVertexPosition;
// attribute vec2 aTextureCoord;

// uniform mat3 projectionMatrix;

// varying vec2 vTextureCoord;

// void main(void) {
//    gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
//    vTextureCoord = aTextureCoord;
// }
precision mediump float;
attribute vec4 a_Position;
uniform mat4 u_ProjMatrix;
attribute vec2 a_TexCoord;
varying vec2 v_TexCoord;
void main() {
   // gl_Position = vec4(u_ProjMatrix * vec4(a_Position, 1.0).xy, 0.0, 1.0);
   gl_Position = u_ProjMatrix * a_Position;
   v_TexCoord = a_TexCoord;//纹理坐标系下的坐标
}