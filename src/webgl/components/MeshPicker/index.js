import { componentType, math } from "@webgl/utils/index.js";
export default class MeshPicker {
    constructor(node) {
        this.calssName = componentType.MeshPicker;
        this.uuid = math.generateUUID(this.calssName);
        console.log("node:", node);
        this.node = node;
    }

    onClick(callBack) {
        this.on("click", callBack);
    }
    on(eventType, callBack) {
        this.node?.gl?.canvas?.addEventListener(eventType, callBack);
        // 绑定事件
    }
}
