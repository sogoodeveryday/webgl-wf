precision highp float;
varying vec2 v_TextureCoord;
uniform sampler2D u_Sampler;
void main(void){
   gl_FragColor = texture2D(u_Sampler, v_TextureCoord);
}