attribute vec3 a_Position;
attribute vec2 a_TextureCoord;
varying vec2 v_TextureCoord;
void main(void) {
   gl_Position = vec4(a_Position, 1.0);
   v_TextureCoord = a_TextureCoord;
}
