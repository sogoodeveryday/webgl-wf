/**
 * Sprite组件提供了在屏幕上渲染精灵（2D图像）的功能。它包括了以下功能：

封装了纹理（Texture）和几何体（Geometry）的渲染逻辑。
可以在屏幕上绘制精灵，并处理精灵的位置、大小、旋转等属性。
可以与相机进行交互，实现精灵的视图变换和投影。
可能还包括处理精灵动画、碰撞检测等功能，具体取决于游戏或应用的需求。
 * 
 */
//绘制点
import { Mesh } from "@webgl/index.js";
import { Material } from "@webgl/index.js";
import { Geometry } from "@webgl/index.js";
import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
export class Sprite {
    constructor(config) {
        this.init(config);
    }
    init(config) {
        let { gl, texture } = config;
        this.gl = gl;
        this.texture = texture; // 纹理
        this.makeMesh(config);
    }
    makeMesh(config) {
        let { gl, texture } = config;
        //贴图坐标
        let a_TextureCoord = [0, 1, 0, 0, 1, 1, 1, 0];
        //顶点坐标
        let a_Position = [-1, 1, 0, -1, -1, 0, 1, 1, 0, 1, -1, 0];
        a_Position = a_Position.map((item) => {
            return item / 2;
        });
        let uniforms = {
            u_Sampler: {
                value: texture,
            },
        };

        //创建集合体
        let geometry = new Geometry({ gl });
        //添加顶点 并设置读取格式
        geometry.setBuffer("a_TextureCoord", { value: a_TextureCoord, size: 2 });
        geometry.setBuffer("a_Position", { value: a_Position, size: 3 });

        let material = new Material({ gl });
        // //创建材质
        material.setGLSLSource(vertexShaderSource, fragmentShaderSource);
        material.setUniformFormat(uniforms);
        // console.log("material:", material);
        //创建材质
        // let material = new Material({
        //     gl,
        //     fragmentShaderSource,
        //     vertexShaderSource,
        //     //传递glsl中的unifrom参数
        //     uniforms,
        // });

        let mesh = new Mesh({ gl, geometry, material });
        this.mesh = mesh;
    }
    update(stage) {
        let gl = stage.gl;
        let drawGeometryType = gl.TRIANGLE_STRIP;
        this.mesh?.update({ gl, drawGeometryType });
    }
}
