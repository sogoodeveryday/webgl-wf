/**
 * Texture组件用于在计算机图形学中表示图像或纹理，并将其应用于几何体以进行渲染。它的作用包括：
存储图像数据，可以是位图、矢量图或其他形式的图像数据。
将图像数据映射到几何体上，以便在渲染过程中呈现出图像的外观。
可以包括纹理过滤、纹理坐标映射、纹理环绕方式等属性，以控制纹理在几何体上的显示效果。
在游戏开发、计算机图形学和计算机辅助设计等领域中被广泛应用。
 * 
 */
//图片
export class Texture {
    constructor({ gl, image }) {
        this.gl = gl;
        this.texture = gl.createTexture();
        this.image = image;
        this.image && this.loadFromImage(image);
    }

    loadFromImage(image) {
        const { gl, texture } = this;
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
        //开始0号纹理通道
        gl.activeTexture(gl.TEXTURE0);
        //绑定texture
        gl.bindTexture(gl.TEXTURE_2D, texture);
        //配置纹理的参数
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    bind = (sampler) => {
        const { gl, texture } = this;
        //绑定纹理
        gl.bindTexture(gl.TEXTURE_2D, texture);
        //设置纹理数据
        gl.uniform1i(sampler, 0);
    };

    destroy() {
        const { gl, texture } = this;
        gl.deleteTexture(texture);
    }
}
