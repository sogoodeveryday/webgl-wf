attribute vec2 a_VertexPosition;
attribute vec2 a_TextureCoord; 
varying vec2 v_TextureCoord;
void main(void) {
   gl_Position = vec4( vec3(a_VertexPosition, 1.0)).xy, 0.0, 1.0);
   v_TextureCoord = a_TextureCoord;
}
