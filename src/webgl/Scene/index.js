/**
 * 当封装一个场景（Scene）组件时，你可以考虑以下功能：

    添加物体：提供方法用于向场景中添加物体，可以是模型、粒子效果等。

    设置相机：提供方法用于设置相机的位置、视角等参数。

    更新场景：提供方法用于更新场景中的物体状态，例如动画效果、交互操作等。

    渲染场景：提供方法用于执行渲染操作，将场景中的物体渲染到屏幕上。
 */
import { componentType, math } from "@webgl/utils/index.js";
export default class Scene {
    constructor() {
        this.className = componentType.scene;
        this.uuid = math.generateUUID(this.calssName);
        this.gameobjects = [];
    }

    add(object) {
        this.gameobjects.push(object);
    }

    update(stage) { 
        // 在这里可以更新场景中的物体状态，例如执行动画、处理交互等
        for (let i = 0; i < this.gameobjects.length; i++) {
            const item = this.gameobjects[i];
            item.update && item.update(stage);
        }
    }
}

// class Scene {
//     constructor() {
//         this.objects = [];
//         this.camera = {
//             position: { x: 0, y: 0, z: 0 },
//             target: { x: 0, y: 0, z: 0 },
//             up: { x: 0, y: 1, z: 0 },
//         };
//     }

//     addObject(object) {
//         this.objects.push(object);
//     }

//     setCamera(position, target, up) {
//         this.camera.position = position;
//         this.camera.target = target;
//         this.camera.up = up;
//     }

//     update() {
//         // 在这里可以更新场景中的物体状态，例如执行动画、处理交互等
//     }

//     render(renderer) {
//         for (let i = 0; i < this.objects.length; i++) {
//             const object = this.objects[i];
//             renderer.renderObject(object, this.camera);
//         }
//     }
// }
