import BaseLight from "./BaseLight"; // 基础光
import DirectionalLight from "./DirectionalLight"; // 平行光
import AmbientLight from "./AmbientLight"; // 环境光
import PointLight from "./PointLight"; // 点光源
export { BaseLight, DirectionalLight, AmbientLight, PointLight };
