import { componentType, math, windowEvent } from "@webgl/utils/index.js";

import { BaseLight } from "./index.js";
export default class AmbientLight {
    constructor(color = [1, 1, 1]) {
        // super();
        this.calssName = componentType.AmbientLight;
        this.color = color;
        this.colorKey = "u_LightColor";
    }
    // 渲染场景
    update = (mesh) => {
        //获取矩阵的相关key 然后赋值
        mesh?.material.setUniformFormat({
            // [this.positionKey]: {
            //     value: this.position,
            // },
            [this.colorKey]: {
                value: this.color,
            },
            // [this.intensityKey]: {
            //     value: this.intensity,
            // },
        });
        // console.log(mesh.material.uniforms.u_LightDirection.value);
    };
}
