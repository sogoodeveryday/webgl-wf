import { mat4, vec3 } from "gl-matrix";
import { Node } from "@webgl/index.js";
import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";
export default class BaseLight {
    constructor() {
        this.position = vec3.fromValues(0, 0, 1);
        this.positionKey = "u_LightDirection";
        this.color = vec3.fromValues(1, 1, 1);
        this.colorKey = "u_LightColor";
        this.intensity = 1;
        this.intensityKey = "u_LightIntensity";
    }
    update = (mesh) => { 
        //获取矩阵的相关key 然后赋值
        mesh?.material.setUniformFormat({
            [this.positionKey]: {
                value: this.position,
            },
            [this.colorKey]: {
                value: this.color,
            },
            // [this.intensityKey]: {
            //     value: this.intensity,
            // },
        });
        // console.log(mesh.material.uniforms.u_LightDirection.value);
    };
}
