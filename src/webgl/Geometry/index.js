import { Node } from "./Node";
import Point from "./Point";
import Line from "./Line";
import Cube from "./Cube";
import Triangles from "./Triangles";
import Rectangle from "./Rectangle";
import { isArrayBuffer } from "@webgl/utils/index.js";

/**
 * 几何体（Geometry）组件通常需要处理以下情况：

设置顶点数据：包括顶点坐标、法线、纹理坐标等
*“几何体”表示一个模型。它由两个部分组成：
*-GeometryStyle-模型的结构，如属性布局
*-GeometryData-模型的数据-它由缓冲区组成。
*这可以包括位置、uvs、法线、颜色等任何内容。
Geometry在WebGL中起着至关重要的作用，它定义了3D对象的外观和行为。通过创建和操作Geometry，您可以实现以下功能：

创建基本形状：您可以使用Geometry来创建基本的3D形状，如立方体、球体、圆柱体等。通过定义顶点的位置和属性，您可以构建出各种形状。

定义顶点属性：Geometry允许您为每个顶点定义属性，如颜色、法线、纹理坐标等。这些属性可以用于实现光照效果、纹理映射等。

进行变换和动画：通过修改Geometry的顶点位置，您可以实现对象的变换，如平移、旋转和缩放。通过在每帧更新Geometry的位置，您可以创建动画效果。

碰撞检测：Geometry还可以用于进行碰撞检测。通过定义对象的边界框或碰撞体积，您可以检测对象之间的碰撞并采取相应的行动。

优化性能：在WebGL中，Geometry的优化对于提高性能至关重要。通过合并顶点、使用索引缓冲区和使用顶点缓冲区对象（VBO），您可以减少数据传输和渲染开销，从而提高性能。

总之，Geometry在WebGL中扮演着定义和操作3D对象的关键角色。通过使用Geometry，您可以创建各种形状、定义属性、实现变换和动画，并优化性能，从而实现令人惊叹的3D图形效果。


这些是几何体组件通常需要处理的情况，具体实现会根据渲染引擎的需求和设计进行扩展和优化。
 * 
 */
import { arrayLength, isObject } from "@webgl/utils/index.js";
class Geometry {
    constructor(config) {
        let { gl } = config;
        this.gl = gl;
        /**
         * 用域储存防止的变量及其对应的相关配置内容
         * 例如 {
         *  a_Position:{
         *    buffer:xx,
         *    size:xx,
         *    ...
         *    }
         * }
         */
        this.vttributeInfo = {};
        this.indexInfo = null; //顶点的相关数据 给上默认值
        // 其他几何体属性的初始化
        this.init(config);
    }
    init({ gl }) {
        this.setGl(gl);
        return this;
    }
    setGl(gl) {
        this.gl = gl;
    }
    /**
     * @param {object} data
     * @param {number} index默认 0 //shader的变量名地址
     * @param number{} size默认 0 几个值为一个单位
     * @param {} type 默认 gl.FLOAT类型
     * @param {boolean} normalized 默认 false 向量归一化
     * @param {number} stride 步进 0,
     * @param {number} offse 偏移量 0,
     * @param {gl.ARRAY_BUFFER || gl.ELEMENT_ARRAY_BUFFER} target 默认gl.ARRAY_BUFFER
     * @param {} drawType 默认gl.STATIC_DRAW,
    //  */
    setVttribute(data) {
        this._setVttributeBufferFormat(data);
    }
    _setVttributeBufferFormat(data) {
        let { gl } = this;
        let { key, format } = data;
        let bufferInfo = this.vttributeInfo[key] || this._setVttributeDefaultFormat(gl.ARRAY_BUFFER);
        this.vttributeInfo[key] = Object.assign(bufferInfo, format);
    }
    setBuffer(name, dataInfo) {
        let { gl } = this;
        let { value } = dataInfo;
        let buffer = this.createArrayBuffer(gl, value);
        this._setVttributeBufferFormat({
            key: name,
            format: { buffer, ...dataInfo },
        });
        return this;
    }
    setIndex(value) {
        let { gl } = this;
        let buffer = this.createIndexbuffer(gl, value);
        this.indexInfo = Object.assign(this.indexInfo || {}, { buffer, target: gl.ELEMENT_ARRAY_BUFFER, value, type: gl.UNSIGNED_BYTE });
        return this;
    }
    /**顶点数据 */
    createArrayBuffer(gl, data) {
        data = this.isArrayBuffer(data, new Float32Array(data));
        return this.createBuffer(gl, gl.ARRAY_BUFFER, data);
    }
    /**index的下角标数据 */
    createIndexbuffer(gl, data) {
        data = this.isArrayBuffer(data, new Uint8Array(data));
        return this.createBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, data);
    }
    //创建buffer
    createBuffer(gl, target, data) {
        const buffer = gl.createBuffer();
        gl.bindBuffer(target, buffer);
        gl.bufferData(target, data, gl.STATIC_DRAW);
        gl.bindBuffer(target, null);
        return buffer;
    }
    /**
     * 往GPU中更新数据
     * @param  {String} name
     * @param  {TypedArray} data
     * @param  {Number} byteOffset = 0
     * @return {Buffer} this
     */
    bufferSubData(name, data, byteOffset = 0) {
        const { gl } = this;
        //得到指定的buffer
        let theBufferInfo = this.vttributeInfo[name] || null;
        //检测是否存在
        if (!theBufferInfo) return console.error(`attribute ${name} is not exist vttributeInfo is`, this.vttributeInfo);
        let { target, buffer } = theBufferInfo;
        //绑定当前的buffer
        this.bind(target, buffer);
        //更新buffer中数据 
        data = this.isArrayBuffer(data, new Float32Array(data)); 
        gl.bufferSubData(target, byteOffset, data);
        return this;
    }
    /**
     * 绑定
     * @param  {gl.ELEMENT_ARRAY_BUFFER||gl.ARRAY_BUFFER} target
     * @param  {WebGLBuffer || null} data
     * @return {Buffer} this
     */
    bind(target, buffer) {
        this.gl.bindBuffer(target, buffer);
        return this;
    }
    /**
     * 默认设置
     * @param {Object} obj
     * @returns
     */
    _setVttributeDefaultFormat(target) {
        let gl = this.gl;
        return {
            index: 0, //shader的变量名地址
            size: 0,
            type: gl.FLOAT,
            normalized: false,
            stride: 0,
            offse: 0,
            target,
            buffer: null,
            drawType: gl.STATIC_DRAW,
        };
    }
    getVttributeInfo(key) {
        return this.vttributeInfo[key];
    }
    isArrayBuffer(array, typeArray) {
        return isArrayBuffer(array) ? array : typeArray;
    }
}

export { Node, Geometry, Point, Line, Cube, Triangles, Rectangle };
