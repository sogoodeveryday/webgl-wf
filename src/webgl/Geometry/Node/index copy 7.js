import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";
import { mat4 } from "gl-matrix";
export class Node {
    constructor() {
        this.transform = {
            position: { x: 0, y: 0, z: 0 },
            rotation: { x: 0, y: 0, z: 0 },
            scale: { x: 0, y: 0, z: 0 },
            opacity: 1,
            color: [1, 1, 1, 1], //颜色默认白色
            anchor: { x: 0, y: 0, z: 0 },
            width: 1, //宽
            height: 1, //高
            depth: 1, //深度
        };
        //组件列表 可以使用多个组件
        this.components = [];
        this.modelMatrix = mat4.create(); //模型矩阵
        this.modelMatrixKey = "a_ModelMatrix";
        this.normalMatrix = mat4.create(); //逆转置矩阵
        this.normalMatrix = "u_NormalMatrix"; //逆转置矩阵
        this.translationMatrix = mat4.create(); //位移矩阵
        this.rotationMatrix = mat4.create(); //旋转矩阵
        this.scaleMatrix = mat4.create(); //缩放矩阵
    }
    set x(x) {
        this.transform.position.x = x;
        // mat4.translate(this.modelMatrix, this.translationMatrix, [x, this.transform.position.y, this.transform.position.z]);
        this._onPosition();
    }
    get x() {
        return this.transform.position.x;
    }
    set y(y) {
        this.transform.position.y = y;
        // mat4.translate(this.modelMatrix, this.translationMatrix, [this.transform.position.x, y, this.transform.position.z]);
        this._onPosition();
    }
    get y() {
        return this.transform.position.y;
    }
    set z(z) {
        this.transform.position.z = z;
        // mat4.translate(this.modelMatrix, this.translationMatrix, [this.transform.position.x, this.transform.position.y, z]);
        this._onPosition();
    }
    get z() {
        return this.transform.position.z;
    }
    set position(position) {
        let data = position;
        if (isArray(position)) data = { x: position[0], y: position[1], z: position[2] };
        let { x = 0, y = 0, z = 0 } = data;
        this.transform.position = { x, y, z };
        // mat4.translate(this.modelMatrix, this.translationMatrix, [x, y, z]);
        this._onPosition();
    }
    get position() {
        return this.transform.position;
    }
    set rotationX(rotationX) {
        this.transform.rotation.x = rotationX;
        // mat4.rotateX(this.modelMatrix, this.modelMatrix, rotationX);
        this._onRotation();
    }
    get rotationX() {
        return this.transform.rotation.x;
    }
    set rotationY(rotationY) {
        this.transform.rotation.y = rotationY;
        // mat4.rotateY(this.modelMatrix, this.modelMatrix, rotationY);
        this._onRotation();
    }
    get rotationY() {
        return this.transform.rotation.y;
    }
    set rotationZ(rotationZ) {
        this.transform.rotation.z = rotationZ;
        // mat4.rotateZ(this.modelMatrix, this.modelMatrix, rotationZ);
        this._onRotation();
    }
    get rotationZ() {
        return this.transform.rotation.z;
    }
    set rotation(rotation) {
        if (isArray(rotation)) rotation = { x: rotation[0], y: rotation[1], z: rotation[2] };
        let { x = 0, y = 0, z = 0 } = rotation;
        this.transform.rotation = { x, y, z };
        // mat4.rotateX(this.modelMatrix, this.rotationMatrix, x);
        // mat4.rotateY(this.modelMatrix, this.rotationMatrix, y);
        // mat4.rotateZ(this.modelMatrix, this.rotationMatrix, z);
        this.setNormalMatrix();
        this._onRotation();
    }
    get rotation() {
        return this.transform.rotation;
    }
    set scaleX(scaleX) {
        this.transform.scale.x = scaleX;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [scaleX, this.transform.scale.y, this.transform.scale.z]);
        this._onScale();
    }
    get scaleX() {
        return this.transform.scale.x;
    }
    set scaleY(scaleY) {
        this.transform.scale.y = scaleY;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [this.transform.scale.x, scaleY, this.transform.scale.z]);
        this._onScale();
    }
    get scaleY() {
        return this.transform.scale.y;
    }
    set scaleZ(scaleZ) {
        this.transform.scale.z = scaleZ;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [this.transform.scale.x, this.transform.scale.y, scaleZ]);
        this._onScale();
    }
    get scaleZ() {
        return this.transform.scale.z;
    }
    set scale(scale) {
        if (isArray(scale)) scale = { x: scale[0], y: scale[1], z: scale[2] };
        let { x = 0, y = 0, z = 0 } = scale;
        this.transform.scale = { x, y, z };
        // mat4.scale(this.modelMatrix, this.modelMatrix, [x, y, z]);
        this._onScale();
    }
    get scale() {
        return this.transform.scale;
    }
    set opacity(opacity) {
        this.transform.opacity = opacity;
        this.onOpacity();
    }
    get opacity() {
        return this.transform.opacity;
    }
    set color(color) {
        let data = color;
        if (isString(color)) data = HEX2RGB(color);
        if (!isArray(color)) return console.error("颜色必须是十六进制格式或者是数组的格式");
        this.transform.color = data;
        this.onColor();
    }
    get color() {
        return this.transform.color;
    }
    set anchorX(anchorX) {
        this.transform.anchor.x = anchorX;
        this.onAnchor();
    }
    get anchorX() {
        return this.transform.anchor.x;
    }
    set anchorY(anchorY) {
        this.transform.anchor.y = anchorY;
        this.onAnchor();
    }
    get anchorY() {
        return this.transform.anchor.y;
    }
    set anchorZ(anchorZ) {
        this.transform.anchor.z = anchorZ;
        this.onAnchor();
    }
    get anchorZ() {
        return this.transform.anchor.z;
    }
    set anchor(anchor) {
        this.transform.anchor = anchor;
        this.onAnchor();
    }
    get anchor() {
        return this.transform.anchor;
    }
    set width(width) {
        this.transform.width = width;
        this.onSize();
    }
    get width() {
        return this.transform.width;
    }
    set height(height) {
        this.transform.height = height;
        this.onSize();
    }
    get height() {
        return this.transform.height;
    }
    set depth(depth) {
        this.transform.depth = depth;
        this.onSize();
    }
    get depth() {
        return this.transform.depth;
    }
    addConmponent(component) {
        this.components.push(component);
    }
    //第一次初始化
    start() {}
    //帧更新
    update() {}
    //销毁
    destroy() {}
    // //值发生改变触发事件
    _onPosition() {
        this._onTransform();
        this.onPosition();
    }
    onPosition() {}
    // //值发生改变触发事件
    _onRotation() {
        this._onTransform();
        this.onRotation();
    }
    onRotation() {}
    // //值发生改变触发事件
    _onScale() {
        this._onTransform();
        this.onScale();
    }
    onScale() {}
    //值发生改变触发事件
    onColor() {}
    //值发生改变触发事件
    onAnchor() {}
    _onTransform() {
        let { scale, position } = this.transform;
        let modelMatrix = mat4.create();
        mat4.identity(modelMatrix); // 初始化为单位矩阵
        this.translationMatrix = mat4.create(); //位移矩阵
        this.rotationMatrix = mat4.create(); //旋转矩阵
        this.scaleMatrix = mat4.create(); //

        mat4.fromScaling(this.scaleMatrix, [scale.x, scale.y, scale.z]);

        // mat4.rotateX(this.modelMatrix, this.rotationMatrix, this.transform.rotation.x);
        // mat4.rotateY(this.modelMatrix, this.rotationMatrix, this.transform.rotation.y);
        // mat4.rotateZ(this.modelMatrix, this.rotationMatrix, this.transform.rotation.z);
        // mat4.fromRotation(this.rotationMatrix, this.transform.rotation.x, [1, 0, 0]);
        // mat4.fromRotation(this.rotationMatrix, this.transform.rotation.y, [0, 1, 0]);
        // mat4.fromRotation(this.rotationMatrix, this.transform.rotation.z, [0, 0, 1]);

        // mat4.translate(this.modelMatrix, this.translationMatrix, [position.x, position.y, position.z]);
       
        // 旋转
        mat4.rotate(this.rotationMatrix, this.rotationMatrix, this.transform.rotation.x, [1, 0, 0]);
        mat4.rotate(this.rotationMatrix, this.rotationMatrix, this.transform.rotation.y, [0, 1, 0]);
        mat4.rotate(this.rotationMatrix, this.rotationMatrix, this.transform.rotation.z, [0, 0, 1]);

        mat4.fromTranslation(this.translationMatrix, [position.x, position.y, position.z]);
        // mat4.multiply(this.modelMatrix, this.modelMatrix, this.rotationMatrix);
        // 平移
        mat4.multiply(modelMatrix, modelMatrix, this.scaleMatrix);
        mat4.multiply(modelMatrix, modelMatrix, this.rotationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, this.translationMatrix);
        this.modelMatrix = modelMatrix;

        this.onTransform();
    }
    onTransform() {}
    //逆转置矩阵的设置
    setNormalMatrix() {
        this.normalMatrix = mat4.create(); //逆转置矩阵
        mat4.invert(this.normalMatrix, this.modelMatrix);
        mat4.transpose(this.normalMatrix, this.normalMatrix);
    }
}
