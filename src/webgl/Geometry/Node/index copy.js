import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";
export class Node {
    constructor() {
        this.transform = {
            position: { x: 0, y: 0, z: 0 },
            rotation: { x: 0, y: 0, z: 0 },
            scale: { x: 0, y: 0, z: 0 },
            opacity: 1,
            color: [1, 1, 1, 1], //颜色默认白色
            anchor: { x: 0, y: 0, z: 0 },
            width: 1, //宽
            height: 1, //高
            depth: 1, //深度
        };
        //组件列表 可以使用多个组件
        this.components = [];
    }
    set x(x) {
        this.transform.position.x = x;
        this.onPosition();
    }
    get x() {
        return this.transform.position.x;
    }
    set y(y) {
        this.transform.position.y = y;
        this.onPosition();
    }
    get y() {
        return this.transform.position.y;
    }
    set z(z) {
        this.transform.position.z = z;
        this.onPosition();
    }
    get z() {
        return this.transform.position.z;
    }
    set position(position) {
        let data = position;
        if (isArray(position)) data = { x: position[0], y: position[1], z: position[2] };
        let { x = 0, y = 0, z = 0 } = data;
        this.transform.position = { x, y, z };
        this.onPosition();
    }
    get position() {
        return this.transform.position;
    }
    set rotationX(rotationX) {
        this.transform.rotation.x = rotationX;
        this.onRotation();
    }
    get rotationX() {
        return this.transform.rotation.x;
    }
    set rotationY(rotationY) {
        this.transform.rotation.y = rotationY;
        this.onRotation();
    }
    get rotationY() {
        return this.transform.rotation.y;
    }
    set rotationZ(rotationZ) {
        this.transform.rotation.z = rotationZ;
        this.onRotation();
    }
    get rotationZ() {
        return this.transform.rotation.z;
    }
    set rotation(rotation) {
        let { x = 0, y = 0, z = 0 } = rotation;
        this.transform.rotation = { x, y, z };
        this.onRotation();
    }
    get rotation() {
        return this.transform.rotation;
    }
    set scaleX(scaleX) {
        this.transform.scale.x = scaleX;
        this.onScale();
    }
    get scaleX() {
        return this.transform.scale.x;
    }
    set scaleY(scaleY) {
        this.transform.scale.y = scaleY;
        this.onScale();
    }
    get scaleY() {
        return this.transform.scale.y;
    }
    set scaleZ(scaleZ) {
        this.transform.scale.z = scaleZ;
        this.onScale();
    }
    get scaleZ() {
        return this.transform.scale.z;
    }
    set scale(scale) {
        this.transform.scale = scale;
        this.onScale();
    }
    get scale() {
        return this.transform.scale;
    }
    set opacity(opacity) {
        this.transform.opacity = opacity;
        this.onOpacity();
    }
    get opacity() {
        return this.transform.opacity;
    }
    set color(color) {
        let data = color;
        if (isString(color)) data = HEX2RGB(color);
        if (!isArray(color)) return console.error("颜色必须是十六进制格式或者是数组的格式");
        this.transform.color = data;
        this.onColor();
    }
    get color() {
        return this.transform.color;
    }
    set anchorX(anchorX) {
        this.transform.anchor.x = anchorX;
        this.onAnchor();
    }
    get anchorX() {
        return this.transform.anchor.x;
    }
    set anchorY(anchorY) {
        this.transform.anchor.y = anchorY;
        this.onAnchor();
    }
    get anchorY() {
        return this.transform.anchor.y;
    }
    set anchorZ(anchorZ) {
        this.transform.anchor.z = anchorZ;
        this.onAnchor();
    }
    get anchorZ() {
        return this.transform.anchor.z;
    }
    set anchor(anchor) {
        this.transform.anchor = anchor;
        this.onAnchor();
    }
    get anchor() {
        return this.transform.anchor;
    }
    set width(width) {
        this.transform.width = width;
        this.onSize();
    }
    get width() {
        return this.transform.width;
    }
    set height(height) {
        this.transform.height = height;
        this.onSize();
    }
    get height() {
        return this.transform.height;
    }
    set depth(depth) {
        this.transform.depth = depth;
        this.onSize();
    }
    get depth() {
        return this.transform.depth;
    }
    addConmponent(component) {
        this.components.push(component);
    }
    //第一次初始化
    start() {}
    //帧更新
    update() {}
    //销毁
    destroy() {}
    //值发生改变触发事件
    onPosition() {}
    //值发生改变触发事件
    onRotation() {}
    //值发生改变触发事件
    onScale() {}
    //值发生改变触发事件
    onColor() {}
    //值发生改变触发事件
    onAnchor() {}
}
