import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";
import { mat4 } from "gl-matrix";
export class Node {
    constructor() {
        this.transform = {
            position: { x: 0, y: 0, z: 0 },
            rotation: { x: 0, y: 0, z: 0 },
            scale: { x: 0, y: 0, z: 0 },
            opacity: 1,
            color: [1, 1, 1, 1], //颜色默认白色
            anchor: { x: 0, y: 0, z: 0 },
            width: 1, //宽
            height: 1, //高
            depth: 1, //深度
        };
        //组件列表 可以使用多个组件
        this.components = [];
        this.modelMatrix = mat4.create(); //模型矩阵
        this.modelMatrixKey = "a_ModelMatrix";
        this.normalMatrix = mat4.create(); //逆转置矩阵
        this.normalMatrix = "u_NormalMatrix"; //逆转置矩阵
        this.translationMatrix = mat4.create(); //位移矩阵
        this.rotationMatrix = mat4.create(); //旋转矩阵
        this.scaleMatrix = mat4.create(); //缩放矩阵
    }
    set x(x) {
        this.transform.position.x = x;
        // mat4.translate(this.modelMatrix, this.modelMatrix, [x, this.transform.position.y, this.transform.position.z]);
        this.translationMatrix[12] = x;
        // this._onPosition();
    }
    get x() {
        return this.transform.position.x;
    }
    set y(y) {
        this.transform.position.y = y;
        // mat4.translate(this.modelMatrix, this.modelMatrix, [this.transform.position.x, y, this.transform.position.z]);
        this.translationMatrix[13] = y;
        // this._onPosition();
    }
    get y() {
        return this.transform.position.y;
    }
    set z(z) {
        this.transform.position.z = z;
        // mat4.translate(this.modelMatrix, this.modelMatrix, [this.transform.position.x, this.transform.position.y, z]);
        this.translationMatrix[14] = z;
        // this._onPosition();
    }
    get z() {
        return this.transform.position.z;
    }
    set position(position) {
        let data = position;
        if (isArray(position)) data = { x: position[0], y: position[1], z: position[2] };
        let { x = 0, y = 0, z = 0 } = data;
        this.transform.position = { x, y, z };
        // mat4.fromTranslation(this.modelMatrix, [x, y, z]);
        // mat4.fromTranslation(this.translationMatrix, [x, y, z]);
        this.x = x;
        this.y = y;
        this.z = z;
        this._onPosition();
    }
    get position() {
        return this.transform.position;
    }
    set rotationX(angle) {
        this.transform.rotation.x = angle;
        mat4.rotateX(this.modelMatrix, this.modelMatrix, angle);
        // mat4.fromRotation(this.rotationMatrix, angle, [1, 0, 0]);
        // this.setNormalMatrix();
        // this._onRotation();
    }
    get rotationX() {
        return this.transform.rotation.x;
    }
    set rotationY(angle) {
        this.transform.rotation.y = angle;
        mat4.rotateY(this.modelMatrix, this.modelMatrix, angle);
        // mat4.fromRotation(this.rotationMatrix, angle, [0, 1, 0]);
        // this.setNormalMatrix();
        // this._onRotation();
    }
    get rotationY() {
        return this.transform.rotation.y;
    }
    set rotationZ(angle) {
        this.transform.rotation.z = angle;
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, angle);
        // mat4.fromRotation(this.rotationMatrix, angle, [0, 0, 1]);
        // this.setNormalMatrix();
        // this._onRotation();
    }
    get rotationZ() {
        return this.transform.rotation.z;
    }
    set rotation(rotation) {
        if (isArray(rotation)) rotation = { x: rotation[0], y: rotation[1], z: rotation[2] };
        let { x = 0, y = 0, z = 0 } = rotation;
        this.transform.rotation = { x, y, z };
        mat4.rotateX(this.modelMatrix, this.modelMatrix, x);
        mat4.rotateY(this.modelMatrix, this.modelMatrix, y);
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, z);
        // mat4.fromRotation(this.rotationMatrix, x, [1, 0, 0]);
        // mat4.fromRotation(this.rotationMatrix, y, [0, 1, 0]);
        // mat4.fromRotation(this.rotationMatrix, z, [0, 0, 1]);
        // this.rotationX = x;
        // this.rotationY = y;
        // this.rotationZ = z;
        // debugger;
        // console.log(this.rotationMatrix);
        this._onRotation();
    }
    get rotation() {
        return this.transform.rotation;
    }
    set scaleX(scaleX) {
        this.transform.scale.x = scaleX;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [scaleX, this.transform.scale.y, this.transform.scale.z]);
        this.scaleMatrix[0] = scaleX; // x轴缩放值
        // this._onScale();
    }
    get scaleX() {
        return this.transform.scale.x;
    }
    set scaleY(scaleY) {
        this.transform.scale.y = scaleY;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [this.transform.scale.x, scaleY, this.transform.scale.z]);
        this.scaleMatrix[5] = scaleY; // x轴缩放值
        // this._onScale();
    }
    get scaleY() {
        return this.transform.scale.y;
    }
    set scaleZ(scaleZ) {
        this.transform.scale.z = scaleZ;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [this.transform.scale.x, this.transform.scale.y, scaleZ]);
        this.scaleMatrix[10] = scaleZ; // z轴缩放值
        // this._onScale();
    }
    get scaleZ() {
        return this.transform.scale.z;
    }
    set scale(scale) {
        if (isArray(scale)) scale = { x: scale[0], y: scale[1], z: scale[2] };
        let { x = 0, y = 0, z = 0 } = scale;
        this.transform.scale = { x, y, z };
        // mat4.scale(this.modelMatrix, this.modelMatrix, [x, y, z]);
        // mat4.fromScaling(this.scaleMatrix, [x, y, z]);
        this.scaleX = x;
        this.scaleY = y;
        this.scaleZ = z;
        this._onScale();
    }
    get scale() {
        return this.transform.scale;
    }
    set opacity(opacity) {
        this.transform.opacity = opacity;
        this.onOpacity();
    }
    get opacity() {
        return this.transform.opacity;
    }
    set color(color) {
        let data = color;
        if (isString(color)) data = HEX2RGB(color);
        if (!isArray(color)) return console.error("颜色必须是十六进制格式或者是数组的格式");
        this.transform.color = data;
        this._onColor();
    }
    get color() {
        return this.transform.color;
    }
    set anchorX(anchorX) {
        this.transform.anchor.x = anchorX;
        this._onAnchor();
    }
    get anchorX() {
        return this.transform.anchor.x;
    }
    set anchorY(anchorY) {
        this.transform.anchor.y = anchorY;
        this._onAnchor();
    }
    get anchorY() {
        return this.transform.anchor.y;
    }
    set anchorZ(anchorZ) {
        this.transform.anchor.z = anchorZ;
        this._onAnchor();
    }
    get anchorZ() {
        return this.transform.anchor.z;
    }
    set anchor(anchor) {
        this.transform.anchor = anchor;
        this._onAnchor();
    }
    get anchor() {
        return this.transform.anchor;
    }
    set width(width) {
        this.transform.width = width;
        this.onSize();
    }
    get width() {
        return this.transform.width;
    }
    set height(height) {
        this.transform.height = height;
        this.onSize();
    }
    get height() {
        return this.transform.height;
    }
    set depth(depth) {
        this.transform.depth = depth;
        this.onSize();
    }
    get depth() {
        return this.transform.depth;
    }
    addConmponent(component) {
        this.components.push(component);
    }
    //第一次初始化
    start() {}
    //帧更新
    update() {}
    //销毁
    destroy() {}
    // //值发生改变触发事件
    _onPosition() {
        this._onTransform();
        this.onPosition();
    }
    onPosition() {}
    // //值发生改变触发事件
    _onRotation() {
        this.setNormalMatrix(); //逆转置矩阵更新
        this._onTransform();
        this.onRotation();
    }
    onRotation() {}
    // //值发生改变触发事件
    _onScale() {
        this._onTransform();
        this.onScale();
    }
    onScale() {}
    //值发生改变触发事件
    _onColor() {
        this.onColor();
    }
    onColor() {}
    //值发生改变触发事件
    _onAnchor() {
        this.onAnchor();
    }
    onAnchor() {}
    //位移 旋转 缩放出发
    _onTransform() {
        // this.translationMatrix = mat4.create();
        mat4.multiply(this.modelMatrix, this.rotationMatrix, this.translationMatrix);
        mat4.multiply(this.modelMatrix, this.modelMatrix, this.scaleMatrix);
        // console.log("this.rotationMatrix:", this.rotationMatrix);
        // mat4.multiply(this.modelMatrix, this.rotationMatrix, this.scaleMatrix);
        // mat4.multiply(this.modelMatrix, this.modelMatrix, this.translationMatrix);
        this.onTransform();
    }
    onTransform() {}
    //逆转置矩阵的设置
    setNormalMatrix() {
        this.normalMatrix = mat4.create(); //逆转置矩阵
        mat4.invert(this.normalMatrix, this.modelMatrix);
        mat4.transpose(this.normalMatrix, this.normalMatrix);
    }
}
