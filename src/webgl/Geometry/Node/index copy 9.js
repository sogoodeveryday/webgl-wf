import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";
import { mat4 } from "gl-matrix";
export class Node {
    constructor() {
        this.transform = {
            position: { x: 0, y: 0, z: 0 },
            rotation: { x: 0, y: 0, z: 0 },
            scale: { x: 1, y: 1, z: 1 },
            opacity: 1,
            color: [1, 1, 1, 1], //颜色默认白色
            anchor: { x: 0, y: 0, z: 0 },
            width: 1, //宽
            height: 1, //高
            depth: 1, //深度
        };
        //组件列表 可以使用多个组件
        this.components = [];
        this.parent = null; //父对象
        this.children = []; // 子对象列表
        this.modelMatrix = mat4.create(); //模型矩阵
        this.modelMatrixKey = "a_ModelMatrix";
        this.normalMatrix = mat4.create(); //逆转置矩阵
        this.normalMatrix = "u_NormalMatrix"; //逆转置矩阵
    }
    set x(x = 0) {
        this.transform.position.x = x;
        // mat4.translate(this.modelMatrix, this.translationMatrix, [x, this.transform.position.y, this.transform.position.z]);
        this._onPosition();
    }
    get x() {
        return this.transform.position.x;
    }
    set y(y = 0) {
        this.transform.position.y = y * 2;
        // mat4.translate(this.modelMatrix, this.translationMatrix, [this.transform.position.x, y, this.transform.position.z]);
        this._onPosition();
    }
    get y() {
        return this.transform.position.y / 2;
    }
    set z(z = 0) {
        this.transform.position.z = z;
        // mat4.translate(this.modelMatrix, this.translationMatrix, [this.transform.position.x, this.transform.position.y, z]);
        this._onPosition();
    }
    get z() {
        return this.transform.position.z;
    }
    set position(position) {
        let data = position;
        if (isArray(position)) data = { x: position[0], y: position[1], z: position[2] };
        let { x = 0, y = 0, z = 0 } = data;
        this.transform.position = { x, y: y * 2, z };
        // mat4.translate(this.modelMatrix, this.translationMatrix, [x, y, z]);
        this._onPosition();
    }
    get position() {
        return this.transform.position;
    }
    set rotationX(rotationX = 0) {
        this.transform.rotation.x = rotationX;
        // mat4.rotateX(this.modelMatrix, this.modelMatrix, rotationX);
        this._onRotation();
    }
    get rotationX() {
        return this.transform.rotation.x;
    }
    set rotationY(rotationY = 0) {
        this.transform.rotation.y = rotationY;
        // mat4.rotateY(this.modelMatrix, this.modelMatrix, rotationY);
        this._onRotation();
    }
    get rotationY() {
        return this.transform.rotation.y;
    }
    set rotationZ(rotationZ = 0) {
        this.transform.rotation.z = rotationZ;
        // mat4.rotateZ(this.modelMatrix, this.modelMatrix, rotationZ);
        this._onRotation();
    }
    get rotationZ() {
        return this.transform.rotation.z;
    }
    set rotation(rotation) {
        if (isArray(rotation)) rotation = { x: rotation[0], y: rotation[1], z: rotation[2] };
        let { x = 0, y = 0, z = 0 } = rotation;
        this.transform.rotation = { x, y, z };
        // mat4.rotateX(this.modelMatrix, this.rotationMatrix, x);
        // mat4.rotateY(this.modelMatrix, this.rotationMatrix, y);
        // mat4.rotateZ(this.modelMatrix, this.rotationMatrix, z);

        this._onRotation();
    }
    get rotation() {
        return this.transform.rotation;
    }
    set scaleX(scaleX = 1) {
        this.transform.scale.x = scaleX;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [scaleX, this.transform.scale.y, this.transform.scale.z]);
        this._onScale();
    }
    get scaleX() {
        return this.transform.scale.x;
    }
    set scaleY(scaleY = 1) {
        this.transform.scale.y = scaleY;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [this.transform.scale.x, scaleY, this.transform.scale.z]);
        this._onScale();
    }
    get scaleY() {
        return this.transform.scale.y;
    }
    set scaleZ(scaleZ = 1) {
        this.transform.scale.z = scaleZ;
        // mat4.scale(this.modelMatrix, this.modelMatrix, [this.transform.scale.x, this.transform.scale.y, scaleZ]);
        this._onScale();
    }
    get scaleZ() {
        return this.transform.scale.z;
    }
    set scale(scale) {
        console.log("scale", scale);
        if (isArray(scale)) scale = { x: scale[0], y: scale[1], z: scale[2] };
        let { x = 1, y = 1, z = 1 } = scale;
        this.transform.scale = { x, y, z };
        // mat4.scale(this.modelMatrix, this.modelMatrix, [x, y, z]);
        this._onScale();
    }
    get scale() {
        return this.transform.scale;
    }
    set opacity(opacity) {
        this.transform.opacity = opacity;
        this.onOpacity();
    }
    get opacity() {
        return this.transform.opacity;
    }
    set color(color) {
        let data = color;
        if (isString(color)) data = HEX2RGB(color);
        if (!isArray(color)) return console.error("颜色必须是十六进制格式或者是数组的格式");
        this.transform.color = data;
        this.onColor();
    }
    get color() {
        return this.transform.color;
    }
    set anchorX(anchorX) {
        this.transform.anchor.x = anchorX;
        this._onAnchor();
    }
    get anchorX() {
        return this.transform.anchor.x;
    }
    set anchorY(anchorY) {
        this.transform.anchor.y = anchorY;
        this._onAnchor();
    }
    get anchorY() {
        return this.transform.anchor.y;
    }
    set anchorZ(anchorZ) {
        this.transform.anchor.z = anchorZ;
        this._onAnchor();
    }
    get anchorZ() {
        return this.transform.anchor.z;
    }
    set anchor(anchor) {
        if (isArray(anchor)) anchor = { x: anchor[0], y: anchor[1], z: anchor[2] };
        let { x = 0, y = 0, z = 0 } = anchor;
        this.transform.anchor = { x, y, z };
        this._onAnchor();
    }
    get anchor() {
        return this.transform.anchor;
    }
    set width(width) {
        this.transform.width = width;
        this.onSize();
    }
    get width() {
        return this.transform.width;
    }
    set height(height) {
        this.transform.height = height;
        this.onSize();
    }
    get height() {
        return this.transform.height;
    }
    set depth(depth) {
        this.transform.depth = depth;
        this.onSize();
    }
    get depth() {
        return this.transform.depth;
    }
    addConmponent(component) {
        this.components.push(component);
    }
    //第一次初始化
    start() {}
    //帧更新
    update() {}
    //销毁
    destroy() {}
    // //值发生改变触发事件
    _onPosition() {
        this._onUpdateTransform();
        this.onPosition();
    }
    onPosition() {}
    // //值发生改变触发事件
    _onRotation() {
        this._onUpdateTransform();
        this.onRotation();
    }
    onRotation() {}
    // //值发生改变触发事件
    _onScale() {
        this._onUpdateTransform();
        this.onScale();
    }
    onScale() {}
    //值发生改变触发事件
    onColor() {}
    //值发生改变触发事件
    _onAnchor() {
        this._onUpdateTransform();
        this.onAnchor();
    }
    onAnchor() {}
    onSize() {}
    _onUpdateTransform() {
        let { scale, position, rotation, anchor } = this.transform;

        mat4.identity(this.modelMatrix); // 重置模型矩阵
        //如果存在父节点就将父节点的数据整合 层级系统
        if (this.parent) {
            mat4.multiply(this.modelMatrix, this.parent.modelMatrix, this.modelMatrix);
        }
        this.setAnchor(anchor);
        this.translateWithAnchor(position);
        this.rotateWithAnchor(rotation);
        this.scaleWithAnchor(scale);
        // // 应用位移
        // mat4.translate(this.modelMatrix, this.modelMatrix, [
        //     position.x,
        //     position.y,
        //     position.z,
        // ]);
        // // 应用旋转
        // mat4.rotateX(this.modelMatrix, this.modelMatrix, rotation.x);
        // mat4.rotateY(this.modelMatrix, this.modelMatrix, rotation.y);
        // mat4.rotateZ(this.modelMatrix, this.modelMatrix, rotation.z);
        // // 应用缩放
        // mat4.scale(this.modelMatrix, this.modelMatrix, [
        //     scale.x,
        //     scale.y,
        //     scale.z,
        // ]);

        this.setNormalMatrix();
        //更新自己的子节点数据
        for (let index = 0; index < this.children.length; index++) {
            const element = this.children[index];
            element._onUpdateTransform();
        }
        this.onUpdateTransform();
    }
    onUpdateTransform() {}
    //逆转置矩阵的设置
    setNormalMatrix() {
        this.normalMatrix = mat4.create(); //逆转置矩阵
        mat4.invert(this.normalMatrix, this.modelMatrix);
        mat4.transpose(this.normalMatrix, this.normalMatrix);
    }
    //添加子节点
    addChild(child) {
        if (child.parent) {
            // 如果子对象已经有父对象，先从原父对象中移除
            child.parent.removeChild(child);
        }
        child.parent = this;
        this.children.push(child);
    }
    // 移除子节点
    removeChild(child) {
        const index = this.children.indexOf(child);
        if (index !== (-1)) {
            child.parent = null;
            this.children.splice(index, 1);
        }
    }
    setAnchor(anchor) {
        // 确保锚点在 [-1, 1] 范围内
        anchor.x = Math.max(-1, Math.min(1, anchor.x));
        anchor.y = Math.max(-1, Math.min(1, anchor.y));
        anchor.z = Math.max(-1, Math.min(1, anchor.z));
    
        // 计算锚点相对于模型尺寸的偏移量
        let offsetX = anchor.x * this.transform.width * 1;
        let offsetY = anchor.y * this.transform.height * 1;
        let offsetZ = anchor.z * this.transform.depth * 1;
    
        // 将模型的原点移动到确定的锚点位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [offsetX, offsetY, offsetZ]);
    }
    
    rotateWithAnchor(rotation) {
        // 将模型的原点移动到确定的锚点位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [
            this.anchor.x * this.transform.width * 1,
            this.anchor.y * this.transform.height * 1,
            this.anchor.z * this.transform.depth * 1,
        ]);
    
        // 进行旋转操作
        mat4.rotateX(this.modelMatrix, this.modelMatrix, rotation.x);
        mat4.rotateY(this.modelMatrix, this.modelMatrix, rotation.y);
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, rotation.z);
    
        // 将模型的原点移回到旋转之前的位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [
            -this.anchor.x * this.transform.width * 1,
            -this.anchor.y * this.transform.height * 1,
            -this.anchor.z * this.transform.depth * 1,
        ]);
    }
    
    scaleWithAnchor(scale) {
        // 将模型的原点移动到确定的锚点位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [
            this.anchor.x * this.transform.width * 1,
            this.anchor.y * this.transform.height * 1,
            this.anchor.z * this.transform.depth * 1,
        ]);
    
        // 进行缩放操作
        mat4.scale(this.modelMatrix, this.modelMatrix, [scale.x, scale.y, scale.z]);
    
        // 将模型的原点移回到缩放之前的位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [
            -this.anchor.x * this.transform.width * 1,
            -this.anchor.y * this.transform.height * 1,
            -this.anchor.z * this.transform.depth * 1,
        ]);
    }
    
    translateWithAnchor(translation) {
        // 将模型的原点移动到确定的锚点位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [
            this.anchor.x * this.transform.width * 1,
            this.anchor.y * this.transform.height * 1,
            this.anchor.z * this.transform.depth * 1,
        ]);
    
        // 进行位移操作
        mat4.translate(this.modelMatrix, this.modelMatrix, [translation.x, translation.y, translation.z]);
    
        // 将模型的原点移回到位移之前的位置
        mat4.translate(this.modelMatrix, this.modelMatrix, [
            -this.anchor.x * this.transform.width * 1,
            -this.anchor.y * this.transform.height * 1,
            -this.anchor.z * this.transform.depth * 1,
        ]);
    }
}
