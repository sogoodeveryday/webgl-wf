// 这个矩形几何体继承自 Geometry 组件，通过传入宽度和高度来创建矩形的顶点数据和索引数据，
// 并调用 setBufferData 方法来设置顶点缓冲和索引缓冲。
// 这样就封装了一个简单的矩形几何体组件。
class RectangleGeometry extends Geometry {
    constructor(gl, width, height) {
      super(gl);
      this.width = width;
      this.height = height;
      this.vertices = [
        -width / 2, height / 2, 0,  // 左上角顶点
        -width / 2, -height / 2, 0, // 左下角顶点
        width / 2, -height / 2, 0,  // 右下角顶点
        width / 2, height / 2, 0    // 右上角顶点
      ];
      this.indices = [
        0, 1, 2,  // 第一个三角形
        0, 2, 3   // 第二个三角形
      ];
      this.setBufferData(this.vertices, this.indices);
    }
  }