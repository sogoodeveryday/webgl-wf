precision mediump float;
attribute vec3 a_Position;
uniform vec3 u_Color;
uniform float u_PointSize;
varying vec4 v_Color;
void main() {
   gl_Position = vec4(a_Position, 1.0);
   gl_PointSize = u_PointSize;
   v_Color = vec4(u_Color, 1.0);
}