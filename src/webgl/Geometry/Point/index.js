//绘制点
import { Node } from "./Node.js";
import { Mesh } from "@webgl/index.js";
import { Material } from "@webgl/index.js";
import { Geometry } from "@webgl/index.js";
import { isString, HEX2RGB, isArray, isObject } from "@webgl/utils/index.js";

// import { Program } from "@webgl/Shader/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
export default class Point extends Node {
    constructor(stage) {
        super();
        this.init(stage);
    }
    init(stage) {
        this.gl = stage.gl;
        this.mesh = {};
        this.makeMesh(stage);
    }
    makeMesh(stage) {
        let { gl } = stage;

        //创建集合体
        this.geometry = new Geometry({ gl });
        this.position = [0, 0, 0]; //设置默认值

        this.material = new Material({ gl, vertexShaderSource, fragmentShaderSource });
        this.color = [1, 1, 1]; //设置默认颜色

        let mesh = new Mesh({ gl, geometry: this.geometry, material: this.material });
        this.mesh = mesh;
    }
    //颜色发生改变时，触发此方法
    onColor() {
        let value = this.color;
        this?.material?.setUniformFormat({
            u_Color: {
                value,
            },
        });
    }
    //顶点坐标发生变化时，触发此方法
    onPosition() {
        let { x, y, z } = this.position;
        this?.geometry?.setBuffer("a_Position", { value: [x, y, z], size: 3 });
    }

    /**
     * 设置点的大小
     */
    set pointSize(value = 10) {
        this?.material?.setUniformFormat({
            u_PointSize: {
                value,
            },
        });
    }
    get pointSize() {
        return this?.mesh?.material?.uniforms?.u_PointSize?.value;
    }
    update(stage) {
        let gl = stage.gl;
        let drawGeometryType = gl.POINT;
        this.mesh?.update({ gl, drawGeometryType });
    }
}
