import { Node } from "./Node";
import Point from "./Point";
/**
 * 几何体（Geometry）组件通常需要处理以下情况：

设置顶点数据：包括顶点坐标、法线、纹理坐标等
*“几何体”表示一个模型。它由两个部分组成：
*-GeometryStyle-模型的结构，如属性布局
*-GeometryData-模型的数据-它由缓冲区组成。
*这可以包括位置、uvs、法线、颜色等任何内容。
Geometry在WebGL中起着至关重要的作用，它定义了3D对象的外观和行为。通过创建和操作Geometry，您可以实现以下功能：

创建基本形状：您可以使用Geometry来创建基本的3D形状，如立方体、球体、圆柱体等。通过定义顶点的位置和属性，您可以构建出各种形状。

定义顶点属性：Geometry允许您为每个顶点定义属性，如颜色、法线、纹理坐标等。这些属性可以用于实现光照效果、纹理映射等。

进行变换和动画：通过修改Geometry的顶点位置，您可以实现对象的变换，如平移、旋转和缩放。通过在每帧更新Geometry的位置，您可以创建动画效果。

碰撞检测：Geometry还可以用于进行碰撞检测。通过定义对象的边界框或碰撞体积，您可以检测对象之间的碰撞并采取相应的行动。

优化性能：在WebGL中，Geometry的优化对于提高性能至关重要。通过合并顶点、使用索引缓冲区和使用顶点缓冲区对象（VBO），您可以减少数据传输和渲染开销，从而提高性能。

总之，Geometry在WebGL中扮演着定义和操作3D对象的关键角色。通过使用Geometry，您可以创建各种形状、定义属性、实现变换和动画，并优化性能，从而实现令人惊叹的3D图形效果。


这些是几何体组件通常需要处理的情况，具体实现会根据渲染引擎的需求和设计进行扩展和优化。
 * 
 */

import { arrayLength, isObject } from "@webgl/utils/index.js";
class Geometry {
    constructor(config) {
        let { gl } = config;
        // var verticesInfo = [], var indicesInfo = [], var uvsInfo = [], var normalsInfo = [], var colors = [], var tangents = [], var skinIndices = [], var skinWeights = [], var groups = []
        this.gl = gl;
        this.verticesInfo = this._setInfoFrom(); // 顶点缓冲
        this.indicesInfo = this._setInfoFrom(gl.ELEMENT_ARRAY_BUFFER); // 索引缓冲
        // this.normalsInfo = this._setInfoFrom(); // 法线数据
        // this.uvsInfo = this._setInfoFrom(); // UV数据
        // 其他几何体属性的初始化
        this.init(config);
    }
    init({ gl, verticesInfo, indicesInfo, normalsInfo, uvsInfo }) {
        this.setGl(gl);
        this.haveArrayValue(verticesInfo) && this.setVertices(verticesInfo);
        return this;
    }
    setGl(gl) {
        if (this.gl == gl) return this;
        this.gl = gl;
        return this;
    }
    /**
     * 默认设置
     * @param {Object} obj
     * @returns
     */
    _setInfoFrom(target) {
        let gl = this.gl;
        target ? target : gl.ARRAY_BUFFER;//默认是顶点
        return {
            value: [],
            format: {
                index: 0, //shader的变量名地址
                size: 0,
                type: gl.FLOAT,
                normalized: false,
                stride: 0,
                offse: 0,
                target: target
            },
        };
    }
    /**
     * 数组中的数据大于0
     * @param {Array} array
     * @returns
     */
    haveArrayValue(array) {
        return arrayLength(array);
    }
    //增加顶点
    addVertices(value) {
        if (!arrayLength) return console.error("请传递顶线的数据");
        this.verticesInfo.value.push(value);
    }
    setVerticesInfo({ vertices = [], format = {} }) {
        if (!arrayLength(vertices)) return console.error("请传入顶点数据，数据为空");
        if (!isObject(format)) return console.error("请传入顶点数据处理方式");
        format = Object.assign(this.verticesInfo.format, format);
        this.verticesInfo = {
            value: vertices,
            format,
        };
        return this;
    }
    // setIndexs(indices = []) {
    //     let bool = this.haveArrayValue(indices);
    //     if (!bool) return console.warn("请传入索引数据，数据为空");
    //     if (this.indices === indices) return this;
    //     this.indices = indices;
    //     return this;
    // }
    // setNormals(normals = []) {
    //     let bool = this.haveArrayValue(normals);
    //     if (!bool) return console.warn("请传入法线数据，数据为空");
    //     if (this.normals === normals) return this;
    //     this.normals = normals;
    //     return this;
    // }
    // setUvs(uvs = []) {
    //     let bool = this.haveArrayValue(uvs);
    //     if (!bool) return console.warn("请传入UV数据，数据为空");
    //     if (this.uvs === uvs) return this;
    //     this.uvs = uvs;
    //     return this;
    // }

    // 其他几何体属性和方法
}

export { Node, Geometry, Point };
