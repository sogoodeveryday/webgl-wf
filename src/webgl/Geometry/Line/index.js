import { Node } from "../index.js";
import { Mesh } from "@webgl/index.js";
import { Material } from "@webgl/index.js";
import { Geometry } from "@webgl/index.js";
// import { Program } from "@webgl/Shader/index.js";
import defaultfragment from "./defaultProgram.frag?raw";
import defaultVertex from "./defaultProgram.vert?raw";

export default class Line {
    constructor(config) {
        let { gl } = config;
        this.x = -1;
        this.init(gl);
    }
    init(gl) {
        let geometry = new Geometry({ gl });
        geometry.setBuffer("a_Position", { value: [this.x, -0.0, 0.0, 0.5, 0.5, 0.0], size: 3 });
        geometry.setBuffer("a_Color", { value: [1, 0, 1, 0, 1, 0], size: 3 });

        let material = new Material({ gl });
        material.setGLSLSource(defaultVertex, defaultfragment);

        let mesh = new Mesh({ gl, geometry, material });
        this.mesh = mesh;
    }
    update(stage) {
        let gl = stage.gl;

        if (this.x > 1) {
            this.x = -1;
        } else {
            this.x += 0.1;
        }
        this.mesh.geometry.bufferSubData("a_Position", [this.x, -0.0, 0.0, -this.x, 0.5, 0.0]);
        // 要绘制的图形
        let drawGeometryType = gl.LINES;
        this.mesh?.update({ gl, drawGeometryType });
    }
}
