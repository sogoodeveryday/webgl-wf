import { Node } from "../index.js";
import { Mesh } from "@webgl/index.js";
import { Material } from "@webgl/index.js";
import { Geometry } from "@webgl/index.js";
// import { Program } from "@webgl/Shader/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
import { Matrix4 } from '@webgl/utils/index'
export default class Cube {
    constructor(config) {
        let { gl } = config;
        console.log('config:',gl.canvas.width,gl.canvas.height);
        this.init(gl);
    }
    init(gl) {
        //顶点的处理
        var verticesBase = [
            1.0, 1.0, 1.0,   // v0 White
            -1.0, 1.0, 1.0,  // v1 Magenta
            -1.0, -1.0, 1.0, // v2 Red
            1.0, -1.0, 1.0,  // v3 Yellow
            1.0, -1.0, -1.0, // v4 Green
            1.0, 1.0, -1.0,  // v5 Cyan
            -1.0, 1.0, -1.0,  // v6 Blue
            -1.0, -1.0, -1.0, // v7 Black
        ];
        let vertices = verticesBase.map((item)=>{
            return item/2
        })

        //颜色
        var color = [
            1.0, 1.0, 1.0,  // v0 White
            1.0, 0.0, 1.0,  //v1 Magenta
            1.0, 0.0, 0.0,  // v2 Red
            1.0, 1.0, 0.0,  // v3 Yellow
            0.0, 1.0, 0.0,  // v4 Green
            0.0, 1.0, 1.0,  // v5 Cyan
            0.0, 0.0, 1.0,  // v6 Blue
            0.0, 0.0, 0.0   // v7 Black
        ]
        // indx
        var indices = new Uint8Array([
            1, 6, 7, 1, 7, 2,    // left 
            7, 4, 3, 7, 3, 2,    // down
            4, 7, 6, 4, 6, 5,    // back
            0, 1, 2, 0, 2, 3,    // front
            0, 3, 4, 0, 4, 5,    // right
            0, 5, 6, 0, 6, 1,    // up
        ]);
        //矩阵的创建
        //矩阵
        var mvpMat4 = new Matrix4();//mvp矩阵
        var projMat4 = new Matrix4();//投影矩阵
        var viewMat4 = new Matrix4();//视图矩阵
        var modelMat4 = new Matrix4();//模型矩阵
        //投影矩阵 设置
        projMat4.setPerspective(30, gl.canvas.width / gl.canvas.height, 1, 100);
        // //视图矩阵 设置
        // viewMat4.setLookAt(1.0, 1.0, 5, 0, 0, 0, 0, 1, 0);
        viewMat4.setLookAt(0.0, 0.0, 1, 0, 0, 0, 0, 1, 0);

        //模型矩阵 设置
        modelMat4.setTranslate(0, 0, -2)
        // //计算出模型视图投影矩阵
        mvpMat4.set(projMat4).multiply(viewMat4).multiply(modelMat4)
        console.log('mvpMat4.elements:',mvpMat4.elements) 
        
        let geometry = new Geometry({ gl });
        geometry.setBuffer("a_Position", { value: vertices, size: 3 });
        geometry.setBuffer("a_Color", { value: color, size: 3 });
        geometry.setIndex(indices); 

        let material = new Material({
            gl,
            fragmentShaderSource,
            vertexShaderSource,
            //传递glsl中的unifrom参数
            uniforms: {
                u_MvpMatrix:{
                    value: projMat4.elements
                },
                u_MvpMatrix2:{
                    value: viewMat4.elements
                },
                u_MvpMatrix3:{
                    value: modelMat4.elements
                }
            },
        });

        let mesh = new Mesh({ gl, geometry, material });
        this.mesh = mesh;
    }
    update(stage) {
        let gl = stage.gl;
        let drawGeometryType = gl.TRIANGLES; 
        this.mesh?.update({ gl, drawGeometryType });
    }
}
