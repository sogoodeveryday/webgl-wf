//绘制点
import { Node } from "../index.js";
import { Mesh } from "@webgl/index.js";
import { Material } from "@webgl/index.js";
import { Geometry } from "@webgl/index.js";
import { isString, HEX2RGB, isArray } from "@webgl/utils/index.js";

// import { Program } from "@webgl/Shader/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
export default class Triangles {
    constructor(stage) {
        this.gl = stage.gl;
        this.init(stage);
    }
    init(stage) {
        this.makeMesh(stage);
    }
    makeMesh(stage) {
        let vertices = [-1, -1, 0, 1, -1, 0, 0, 1, 0];
        vertices = vertices.map((item) => {
            return item / 2;
        });
        let indices = [0, 1, 2];
        let { gl, uniforms } = stage;
        //设置默认值
        uniforms = uniforms || {
            u_Color: {
                value: [1, 1, 1],
            }, 
        };

        //创建集合体
        let geometry = new Geometry({ gl });
        //添加顶点 并设置读取格式
        geometry.setBuffer("a_Position", { value: vertices, size: 3 });
        //创建材质
        let material = new Material({
            gl,
            fragmentShaderSource,
            vertexShaderSource,
            //传递glsl中的unifrom参数
            uniforms,
        });
        let mesh = new Mesh({ gl, geometry, material });
        this.mesh = mesh;
    }
    update(stage) {
        let gl = stage.gl;
        let drawGeometryType = gl.TRIANGLES;
        this.mesh?.update({ gl, drawGeometryType });
    }
}
