/**
 * 渲染器组件主要完成以下工作：

    创建一个 WebGL 上下文，并在构造函数中检查浏览器是否支持 WebGL。

    提供方法 setSize 用于设置渲染画布的大小。

    提供方法 render 用于渲染场景，包括清除画布、遍历场景中的物体并应用相机变换、应用材质、绘制几何体等操作。

    最后调用 gl.finish() 完成渲染。
 */

export default class Renderer {
    constructor(config) {
        console.log(config);
        this.className = "Renderer";
        //场景列表
        this.stageList = [];
        this.startTime = Date.now();
    }
    /**
     * 加载场景 可以是单个或者多个
     * 添加舞台 因为有些需求需要多个不同得canvas
     * @param {Array} stag
     */
    addStage(stage) {
        if (stage instanceof Array) {
            this.stageList.push([...stage]);
        } else {
            this.stageList.push(stage);
        }
    }
    /**
     * 移除舞台
     * 单个移除或者多个移除都可以
     * @param {stag} stag
     */
    remove(stage) {
        //要移除的场景列表
        let removeList = [];
        //key value的形势储存起来 把时间复杂度O^2改为O
        let removeObj = {};
        if (stag instanceof Array) {
            removeList.push([...stage]);
        } else {
            removeList.push(stage);
        }
        //将要移除的以key value的形势收集起来
        for (let i = 0; i < removeList.length; i++) {
            let item = removeList[i];
            removeObj[item.uuid] = item;
        }
        for (let i = 0; i < this.stageList.length; i++) {
            let item = this.stageList[i];
            //遍历要移除的物体，如果存在，则移除
            if (removeObj[item.uuid]) {
                //移除
                this.stageList.splice(i, 1);
                delete removeObj[item.uuid];
                break;
            }
        }
        removeObj = null;
    }
    //开始渲染
    render() {
        // debugger
        this.update();
        // this.measureFrameRate();
        requestAnimationFrame(this.render.bind(this));
        // setInterval(()=>{
        //     // console.log('开始更新')
        //     this.update();
        // },2000)
    }

    measureFrameRate() {
        let endTime = Date.now();
        let elapsedTime = endTime - this.startTime;
        let frameRate = 1000 / elapsedTime;
        console.log("Frame rate: " + frameRate);
        this.startTime = Date.now();
    }
    //帧更新
    update() {
        for (let i = 0; i < this.stageList.length; i++) {
            let item = this.stageList[i];
            item.update(item);
        }
    }
}
