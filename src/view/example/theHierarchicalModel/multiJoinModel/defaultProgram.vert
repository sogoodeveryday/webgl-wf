precision mediump float;
attribute vec4 a_Position;
attribute vec4 a_Color;
attribute vec4 a_Normal;//法向量
uniform mat4 u_ProjMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_ModelMatrix;
uniform vec3 u_LightColor;//光线颜色
uniform vec3 u_LightDirection; //归一化的世界坐标
uniform vec3 u_AmbientLight;//环境光颜色
uniform mat4 u_NormalMatrix;//用来变换法向量的模型逆转置矩阵
varying vec4 v_Color;
void main() {
   gl_Position = u_ProjMatrix * u_viewMatrix * u_ModelMatrix * a_Position;
   //归一化法向量
   vec3 normal = normalize(vec3(u_NormalMatrix * (a_Normal)));
   //归一化的灯光的世界坐标 
   //计算光线方向和法向量的点积
   float nDotL = max(dot(u_LightDirection, normal), 0.0);
   vec3 diffuse = u_LightColor * vec3(a_Color) * nDotL;
   vec3 ambient = u_AmbientLight * a_Color.rgb;
    //计算漫反射光照
   // vec3 ambien =vec3(1,1,1)  * vec3(a_Color);
   // v_Color = vec4(diffuse + ambien, a_Color.a);
   v_Color = vec4(ambient + diffuse, a_Color.a);
}
// precision mediump float;
// attribute vec4 a_Position;
// attribute vec4 a_Color;//基底颜色
// attribute vec4 a_Normal;//法向量
// uniform mat4 u_ProjMatrix;
// uniform mat4 u_viewMatrix;
// uniform mat4 u_ModelMatrix;
// uniform vec3 u_LightColor;//光线颜色
// uniform vec3 u_LightDirection;//归一化的世界坐标 光线向量
// varying vec4 v_Color;
// void main() {
//    gl_Position = u_ProjMatrix * u_viewMatrix * u_ModelMatrix  * a_Position;
//    vec3 normal = normalize(vec3(a_Normal));//法向量归一化
//    float nDotL = max(dot(u_LightDirection, normal), 0.0);//计算光线方向与法向量的点积 为了求出来cosX
//    vec3 diffuse = u_LightColor * vec3(a_Color) * nDotL;//得到漫反射的颜色
//    v_Color = vec4(diffuse, a_Color.a);
// }