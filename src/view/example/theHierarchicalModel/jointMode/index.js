import {
    Mesh,
    Material,
    Geometry,
    OrthographicCamera,
    Node,
} from "@webgl/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
import { Matrix4, createCube } from "@webgl/utils/index";
import { mat4, vec3 } from "gl-matrix";
export default class Cube extends Node {
    constructor(config) {
        super();
        let { gl, width, height, depth } = config;
        this.orthographicCamera = null;
        this.geometry = null; //几何图形
        this.material = null; //材质
        this.mesh = null; //网格
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.init(config);
    }
    init({ gl, width = 1, height = 1, deep = 1 }) {
        // 创建正方体，获取正方体的顶点 颜色 法向量 index的参数
        let { vertices, indices, colors, normals } = createCube(
            width,
            height,
            deep
        );
        // 创建几何体
        let geometry = (this.geometry = new Geometry({ gl }));
        geometry.setBuffer("a_Position", vertices);
        geometry.setBuffer("a_Normal", normals);
        geometry.setBuffer("a_Color", colors);
        geometry.setIndex(indices);

        // 创建材质
        let material = (this.material = new Material({
            gl,
            fragmentShaderSource,
            vertexShaderSource,
        }));

        let mesh = (this.mesh = new Mesh({ gl, geometry, material }));
        this.mesh = mesh;
    }
    onPosition() {
        this.transformChange();
    }
    onRotation() {
        this.transformChange();
    }
    onScale() {
        this.transformChange();
    }
    transformChange() {
        if (!this.material) return;
        let modelMatrix = this.modelMatrix;
        this.material.setUniformFormat({
            u_ModelMatrix: {
                value: modelMatrix,
            },
            u_NormalMatrix: {
                value: this.normalMatrix, //逆转置矩阵
            },
        });
    }
    update(stage) {
        let { gl } = stage;
        let drawGeometryType = gl.TRIANGLES;
        this.mesh?.update({ ...stage, drawGeometryType });
    }
}
