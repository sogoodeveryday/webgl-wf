precision mediump float;
attribute vec4 a_Position;
attribute vec4 a_Color;
uniform mat4 u_MvpMatrix;
uniform mat4 orthoMatrix;
uniform mat4 viewMatrix;
varying vec4 v_Color;
void main() {
   gl_Position = orthoMatrix * viewMatrix * a_Position;
   v_Color = a_Color;
}