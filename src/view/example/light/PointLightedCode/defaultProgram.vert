precision mediump float;
attribute vec4 a_Position;
attribute vec4 a_Color;
attribute vec4 a_Normal;//法向量
uniform mat4 u_ProjMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_ModelMatrix;
uniform vec3 u_LightColor;//光线颜色
uniform vec3 u_LightDirection; //归一化的光的世界坐标
uniform vec3 u_AmbientLight;//环境光颜色
uniform mat4 u_NormalMatrix;//用来变换法向量的模型逆转置矩阵
varying vec4 v_Color;
void main() {
   
   //将模型坐标转化到世界坐标
   vec4 vertexPositon = u_ModelMatrix * a_Position; 
   gl_Position = u_ProjMatrix * u_viewMatrix * vertexPositon;
   //计算出灯光等向量 归一化的灯光方向向量
   vec3 lightDirection = normalize(u_LightDirection - vertexPositon.xyz);
   //归一化法向量
   vec3 normal = normalize(vec3(u_NormalMatrix * (a_Normal)));
   //归一化的灯光的世界坐标 
   //计算光线方向和法向量的点积
   float nDotL = max(dot(lightDirection, normal), 0.0);
   vec3 diffuse = u_LightColor * vec3(a_Color) * nDotL;
   vec3 ambient = u_AmbientLight * a_Color.rgb;
    //计算漫反射光照 
   v_Color = vec4(ambient + diffuse, a_Color.a);
}