precision mediump float;
uniform vec3 u_LightColor;//光线颜色
uniform vec3 u_LightDirection; //归一化的光的世界坐标
uniform vec3 u_AmbientLight;//环境光颜色
varying vec4 v_Color;
varying vec3 v_Normal;//法向量
varying vec3 v_Position;
void main() {
   //把法向量归一化
   vec3 normal = normalize(v_Normal);
   //计算物体指向灯光的向量
   vec3 lightDirection = normalize(u_LightDirection - v_Position);
   //计算漫反射光的颜色
   float nDotL = max(dot(lightDirection, normal), 0.0);
   vec3 diffuse = u_LightColor * v_Color.rgb * nDotL;
   vec3 ambient = u_AmbientLight * v_Color.rgb;
   gl_FragColor = vec4(diffuse + ambient, v_Color.a);
   // v_Position = u_ModelMatrix * a_Position; 
   // gl_Position = u_ProjMatrix * u_viewMatrix * v_Position;
   // //计算出灯光等向量 归一化的灯光方向向量
   // vec3 lightDirection = normalize(u_LightDirection - v_Position.xyz);
   // //归一化法向量
   // vec3 normal = normalize(vec3(u_NormalMatrix * (a_Normal)));
   // //归一化的灯光的世界坐标 
   // //计算光线方向和法向量的点积
   // float nDotL = max(dot(lightDirection, normal), 0.0);
   // vec3 diffuse = u_LightColor * vec3(a_Color) * nDotL;
   // 

   //  //计算漫反射光照 
   // v_Color = vec4(ambient + diffuse, a_Color.a);

   // gl_FragColor = v_Color;
}