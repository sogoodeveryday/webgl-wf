precision mediump float;
attribute vec4 a_Position;
attribute vec4 a_Color;
attribute vec4 a_Normal;//法向量
uniform mat4 u_ProjMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_ModelMatrix;
uniform mat4 u_NormalMatrix;//用来变换法向量的模型逆转置矩阵
varying vec4 v_Color;
varying vec3 v_Normal;//法向量
varying vec3 v_Position;
void main() {
   gl_Position = u_ProjMatrix * u_viewMatrix * u_ModelMatrix * a_Position; 
   //将模型坐标转化到世界坐标
   v_Position = vec3(u_ModelMatrix * a_Position);
   //归一化法向量
   v_Normal = normalize(vec3(u_NormalMatrix * (a_Normal))); 
    //计算漫反射光照 
   v_Color = a_Color;
}