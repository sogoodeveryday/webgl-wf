import { Mesh, Material, Geometry, Node } from "@webgl/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
import { createCube } from "@webgl/utils/index";
export default class Cube extends Node {
    constructor(config) {
        super(config);
        let { gl, texture = null } = config;
        this.orthographicCamera = null;
        this.geometry = null; //几何图形
        this.material = null; //材质
        this.mesh = null; //网格
        this.texture = texture;
        this.init(config);
    }
    init({ gl, width, height, depth }) {
        // 创建正方体，获取正方体的顶点 颜色 法向量 index的参数
        let { vertices, indices, texCoords } = createCube(width, height, depth);
        // 创建几何体
        console.log("vertices:", vertices);
        let geometry = (this.geometry = new Geometry({ gl }));
        geometry.setBuffer("a_Position", vertices);
        geometry.setBuffer("a_TexCoord", texCoords);
        geometry.setIndex(indices);

        // 创建材质
        let material = (this.material = new Material({
            gl,
            fragmentShaderSource,
            vertexShaderSource,
        }));

        let mesh = (this.mesh = new Mesh({ gl, geometry, material }));
        this.mesh = mesh;
    }
    setTexture(texture) {
        this.texture = texture;
        this.onUpdateTransform()
    }
    // onPosition() {
    //     this.onChange();
    // }
    // onRotation() {
    //     this.onChange();
    // }
    // onScale() {
    //     this.onChange();
    // }
    onUpdateTransform() {
        if (!this.material) return;
        let modelMatrix = this.modelMatrix;
        let texture = this.texture;
        let data = {
            u_ModelMatrix: {
                value: modelMatrix,
            },
        };
        if (texture) {
            data["u_image"] = {
                value: texture,
            };
        } 
        console.log("onUpdateTransform:", this.modelMatrix);
        console.log("texture:", this.texture);
        this.material.setUniformFormat(data);
    }
    update(stage) {
        let { gl, camera } = stage;
        let drawGeometryType = gl.TRIANGLES;
        this.mesh?.update({ gl, camera, drawGeometryType });
    }
}
