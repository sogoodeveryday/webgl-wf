precision mediump float;
attribute vec4 a_Position;
attribute vec2 a_TexCoord;
uniform mat4 u_ProjMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_ModelMatrix;
varying vec2 v_TexCoord;
void main() {
   gl_Position = u_ProjMatrix * u_viewMatrix * u_ModelMatrix * a_Position;
   v_TexCoord = a_TexCoord;
}