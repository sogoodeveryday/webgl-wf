import { Mesh, Material, Geometry, OrthographicCamera, Node } from "@webgl/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
import { Matrix4, createCube } from "@webgl/utils/index";
import { mat4 } from "gl-matrix";
export default class Cube extends Node {
    constructor(config) {
        super();
        let { gl } = config;
        this.orthographicCamera = null;
        this.material = null;
        console.log("config:", gl.canvas.width, gl.canvas.height);
        this.init(gl);
    }
    init(gl) {
        // 创建正方体，获取正方体的顶点 颜色 法向量 index的参数
        let { vertices, indices, colors } = createCube(1, 1, 1);

        // 创建几何体
        let geometry = new Geometry({ gl });
        geometry.setBuffer("a_Position", vertices);
        geometry.setBuffer("a_Color", colors);
        geometry.setIndex(indices);

        // 创建材质
        let material = (this.material = new Material({
            gl,
            fragmentShaderSource,
            vertexShaderSource,
        }));

        let mesh = new Mesh({ gl, geometry, material });
        this.mesh = mesh;
    }
    onPosition() {
        if (!this.material) return;
        let modelMatrix = this.modelMatrix;
        this.material.setUniformFormat({
            u_ModelMatrix: {
                value: modelMatrix,
            },
        });
    }
    onRotation() {
        if (!this.material) return;
        let modelMatrix = this.modelMatrix;
        this.material.setUniformFormat({
            u_ModelMatrix: {
                value: modelMatrix,
            },
        });
    }
    onScale() {
        if (!this.material) return;
        let modelMatrix = this.modelMatrix;
        this.material.setUniformFormat({
            u_ModelMatrix: {
                value: modelMatrix,
            },
        });
    }

    update(stage) {
        let { gl, camera } = stage;
        let drawGeometryType = gl.TRIANGLES;
        this.mesh?.update({ gl, camera, drawGeometryType });
    }
}
