import { Mesh, Material, Geometry, OrthographicCamera } from "@webgl/index.js";
import fragmentShaderSource from "./defaultProgram.frag?raw";
import vertexShaderSource from "./defaultProgram.vert?raw";
import { Matrix4, createCube } from "@webgl/utils/index";
import { mat4 } from "gl-matrix";
export default class Cube {
    constructor(config) {
        let { gl } = config;
        console.log("config:", gl.canvas.width, gl.canvas.height);
        this.init(gl);
    }
    init(gl) {
        //
        let { vertices, indices, colors } = createCube(1, 1, 1);
        // 定义视点的位置
        let eye = [1.0, 1.0, 5]; // 视点的位置
        let center = [0, 0, 0]; // 视点的焦点
        let up = [0, 1, 0]; // 视点的上方向
        //找到比例值
        let wh = gl.canvas.width / gl.canvas.height;
        // 定义正射投影矩阵的参数
        let left = -wh;
        let right = wh;
        let bottom = -1;
        let top = 1;
        let near = 0.1;
        let far = 100.0;
        let orthographicCamera = new OrthographicCamera(left, right, bottom, top, near, far); 
        orthographicCamera.position = eye;
        orthographicCamera.lookAt = center;
        orthographicCamera.up = up; 

        let geometry = new Geometry({ gl });
        geometry.setBuffer("a_Position", vertices);
        geometry.setBuffer("a_Color", colors);
        geometry.setIndex(indices);

        let material = new Material({
            gl,
            fragmentShaderSource,
            vertexShaderSource,
            //传递glsl中的unifrom参数
            uniforms: {
                orthoMatrix: {
                    value: orthographicCamera.orthoMatrix,
                },
                viewMatrix:{
                    value: orthographicCamera.viewMatrix
                }
            },
        });

        let mesh = new Mesh({ gl, geometry, material });
        this.mesh = mesh;
    }
    update(stage) {
        let gl = stage.gl;
        let drawGeometryType = gl.TRIANGLES;
        this.mesh?.update({ gl, drawGeometryType });
    }
}
