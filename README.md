编写一个 WebGL 框架通常需要以下组件：

渲染器（Renderer）  初步完成    
场景（Scene）       初步完成    
相机（Camera）      初步完成            
材质（Material）    初步完成    
几何体（Geometry）  初步完成      
着色器（Shader）    未          
灯光（Light）       初步完成          
纹理（Texture）     初步完成        
控制器（Controller）未  鼠标左键 选中物体后默认以选中的物体为中心点  保持按下右键拖动鼠标则物体（选中的）锚点为圆心，半径则有鼠标滚动周决定半径        
动画（Animation）   未      
这些组件可以帮助你构建一个完整的 WebGL 框架。   



       

## 渲染器（Renderer）
渲染器是 WebGL 框架的入口，它负责管理 WebGL 上下文，以及 WebGL 对象的生命周期。  

渲染器通常需要管理以下对象：  

WebGL 上下文（Context）  
着色器（Shader）  
纹理（Texture）  
几何体（Geometry）  
材质（Material）  
相机（Camera）  
灯光（Light）  
控制器（Controller）  
动画（Animation）  

## 控制器      
对于处理事件的组件，特别是在WebGL框架中，需要考虑以下几个方面：     
1. 模型的拾取（Picking）：这涉及到将鼠标点击的屏幕坐标转换为3D世界坐标，并且判断点击位置是否与模型相交。你可以实现射线与模型的碰撞检测，或者使用辅助的库来简化这个过程。        
2. 视角的控制：这包括鼠标拖拽改变视角的角度、缩放操作等。你可能需要实现相机的控制，包括旋转、平移、缩放等操作，以及将鼠标事件转换为相机的变换操作。     
3. 其他交互操作：比如点击按钮、拖拽元素等交互操作，你可能需要实现一套通用的事件处理机制，以便于在WebGL场景中处理这些交互操作。      
在实现这些功能时，你可能需要考虑使用现有的库或者工具来简化开发过程，比如Three.js、A-Frame等。这些库提供了丰富的交互功能和事件处理机制，可以帮助你更快速地实现所需的功能。       
另外，需要设计一个良好的组件架构，以便于将事件处理的逻辑与渲染逻辑分离，从而提高代码的可维护性和可扩展性。

# time

<!-- 游戏是劳累中的休息，热不是休息中的劳累 -->



### 顶点的变量是指定的a_Position
> 因为drawArrays在渲染的时候，需要知道到哪个是顶点顶点的变量,然后获取值中的个数才能知道需要渲染几个点。     
> 当然你也可以，使用drawElements来绘制，只需要给setIndex.setIndex([...])赋值        
                      
### 投影矩阵与视图矩阵的key值 默认是u_ProjMatrix 与 u_viewMatrix。
> 但是可以通过canmera.projMatrixKey与canmera.viewMatrixKey来修改。


24/1/22
    1:正在处理控制库的功能

24/1/12
    1:添加层级系统
    2:添加锚点系统
    3:增加立方体贴图按钮 

24/1/5
    1:添加了光源-点光源 
    2:添加了光源-环境光 
    3:下一步处理下Node组件 关于位移变换，然后是旋转变换，最后是缩放变换

24/1/4
    1:添加光源-平行光

24/1/2
    1：完善Node基础组件 将模型矩阵加入到组件之中
    2：将OrthographicCamera与PerspectiveCamera的案例中 继承Node节点，提供位移 放大 缩小的基础功能

23/12/29 
    1:添加了OrthographicCamera正射投影矩阵组件
    2:添加了PerspectiveCamera透视投影矩阵组件
    3:添加了上述个组件例子

23/12/20    
    1:在Geometry中添加了buffrtSubData方法，用于修改顶点数据，但是这个方法需要传入一个顶点索引，用于确定修改哪个顶点。


23/12/19
    1:打算处理相机组件，处理思路如下              
    首先需要有相应的矩阵，在shader中 然后几何图像需要有自己的默认值，
    然后相机组件会提供统一的相机矩阵值，在页面赋值的时候覆盖几何自己的值。实现相机

23/12/15(里程碑)
    1:封装了Texture与Sprite组件，已经能够初步绘制出来图片了               
    2:修改mesh渲染的bug     
    3:添加了Loader的用于下载图片素材使用       

23/12/08        
    1:封装了line组件        
    2:封装了triangle组件        
    3:封装了rectangle组件       

23/12/01        
    1:使用set get 封装了下Point组件 提供了 color pointSize position的属性       

23/12/01        
    1:point组件的初步成型       
    2:完善乐Material组件        
    3:加入了cobe组件 绘制成功       
    
23/11/30    
    1.line组件绘制成功  
    2.添加point组件,在绘制pont的过程中需要处理uniform的变量 
    3.所以接下来重点是  
    4.开始处理uniform的相关变量的赋值与绘制     

23/11/29    
    添加了line组件  

23/11/23 -- 23/11/28    
    更改了Geometry与Material之间的饿分工    
    Mesh不再处理buffer,由Geometry处理   

    Material 负责   
    1：接受shader glsl的文本    
    2：创建progaram     
    3：将shader glsl的文本中的的变量收集起来    

    Geometry 负责       
    1：接受Mesh的顶点数据及其他的数据 对index与attribute的变量处理  
    2：使用setBuffer给glsl的文本中的的变量传递数值  
    3：创建buffer   
    4：将buffer绑定到shader中   
    5：将顶点数据传入buffer      

    Mesh 负责       
    1：接受Geometry的顶点数据   
    2: 接受Material中的数据     
    3：将Geometry的格式内容与Material中的变量组合，并把内容给了Material     
    4：绘制组合后的数据     
    5：将Mesh的绘制发放给给Renderer.update调取绘制      

23/11/22        
    添加了各个组件      

23/11/21            
    添加处理的骨架      

23/11/20        
    开始着手制作            


     

    













