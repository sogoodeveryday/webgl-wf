const fs = require('fs');
export function viteShader(options) {
    let config = {
        include: [/.fra$g/, /.vert$/],
    }
    options = Object.assign(config, options)
    console.log('options')
    return {
        name: 'vite-shader', 
        transform(CODE, ID) {
            // if(/.js$/.test(ID)){
            //     console.log('js',CODE)
            // } 
            let bool = isNeedPath(options, ID);
            if (!bool) return
            let code = fs.readFileSync(ID, 'utf-8')
            isRaw = id.endsWith("?raw");
            // const buf = fs.readFileSync(id);
            // code = 'export default let shader = `' + code + '`'
            //   code = 'export default let shader = `' + code + '`'
            // code = `export default (function(){return ${code}})()`
            // console.log('code:', code)
            console.log(typeof CODE)
            return {
                code: `export default \`${code}\``,
                map: null // TODO
            };


        }, 
    }
    function isNeedPath(options, ID) {
        let { include } = options
        let bool = false;
        for (let i = 0; i < include.length; i++) {
            let regexp = include[i]
            if (regexp.test(ID)) {
                bool = true;
                return bool;
            }
        }
        return bool;
    }
} 