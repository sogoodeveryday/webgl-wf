编写一个 WebGL 框架通常需要以下组件：

渲染器（Renderer）
场景（Scene）
相机（Camera）
材质（Material）
几何体（Geometry）
着色器（Shader）
灯光（Light）
纹理（Texture）
控制器（Controller）
动画（Animation）
这些组件可以帮助你构建一个完整的 WebGL 框架。

## 渲染器（Renderer）

渲染器是 WebGL 框架的入口，它负责管理 WebGL 上下文，以及 WebGL 对象的生命周期。

```js
class Renderer {
    constructor(canvas) {
        this.canvas = canvas;
        this.gl = canvas.getContext('webgl');
        this.gl.clearColor(0, 0, 0, 0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    clear() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    createProgram(vertexShader, fragmentShader) {
        const gl = this.gl;
        const program = gl.createProgram();
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);
        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            throw new Error('Unable to initialize the shader program: ' + gl.getProgramInfoLog(program));
        }
        return program;
    }

    createShader(type, source) {
        const gl = this.gl;
        const shader = gl.createShader(type);
        gl.shaderSource(shader, source);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            throw new Error('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
        }
        return shader;
    }

    createBuffer(data) {
        const gl = this.gl;
        const buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
        return buffer;
    }

    createVertexArray() {
    }

    createTexture() {
    }

    createFramebuffer() {
    }

    createRenderbuffer() {
    }

    createVertexBuffer(data) {
    }

    createIndexBuffer(data) {
    }

    createUniformBuffer(data) {
    }

    createProgram() {
    }

    createShader(type, source) {
    }

    createBuffer(data) {
    }

    createVertexArray() {
    }
```

    画布缩放器

当画布在屏幕空间中渲染时，画布尺寸一定能正好适配屏幕尺寸吗？当二者不一致时，该怎么进行缩放？
画布缩放器（Canvas Scaler 组件），提供了三种缩放模式来适配不同的需求：

1. Constant Pixel Size：在此模式下，UI 元素的大小将不受 Canvas 的缩放影响，而是保持固定的像素大小。这种模式适用于需要确保 UI 元素在不同设备上的大小保持一致的情况。

2. Scale With Screen Size：在此模式下，UI 元素的大小将根据 Canvas 的缩放比例进行缩放，以适应不同分辨率的设备。这种模式适用于需要在不同分辨率的设备上呈现一致的 UI 布局和视觉效果的情况。

3. Constant Physical Size：在此模式下，UI 元素的大小将根据屏幕的物理大小进行缩放，以确保 UI 元素在不同设备上具有相同的物理大小。这种模式适用于需要确保 UI 元素在不同设备上具有相同的物理大小的情况，例如在使用触摸屏幕的设备上。

为什么有 Constant Pixel Size ，还需要 Constant Physical Size ？
因为不同设备上的 1 像素，实际大小是不同的，实际大小是由设备像素比（DPR）和每英寸像素（PPI）共同决定的。

Scale With Screen Size 的计算公式研究
Scale Witdh Screen Size 主要用于：宽高比的适配。设想一下，原始 UI 与屏幕的宽高比一致，那么很简单，无需适配，就是原始 UI 缩放即可。但是，如果宽高比不一致呢？该放大或缩小多少倍？
这就是 Scale With Screen Size 想解决的问题。

场景假设：
原始屏幕 100* 100 横屏 200*100 竖屏 100\*200

权重weight /屏幕分辨率 原始屏幕 500*500 横屏 1000*500 竖屏 500*1000
Match: 0 [见图5] 横向：widthRatio = 1000 / 500 = 2纵向：heightRatio = 500 / 500 = 1加权平均widthRatio * (1-weight) + heightRatio _ weight = 2所以最终缩放是 2。[见图6] 横向：witdhRatio = 500 / 500纵向：heightRatio = 1000 / 500widthRatio _ ( 1 - weight) + heightRatio \* weight = 1
[见图7]
Match: 0.5 ... ... ...
Match: 1 ... ... ...
